// This file is part of the Eccoin project
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2017 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_CONSENSUS_CONSENSUS_H
#define BITCOIN_CONSENSUS_CONSENSUS_H

#include <inttypes.h>

/** The maximum allowed size for a serialized block, in bytes (network rule) */
static const uint64_t MAX_BLOCK_SIZE = 1000000; // 1MB
static const uint64_t DEFAULT_LARGEST_TRANSACTION = 1000000; // 1MB

static const int64_t nMaxClockDrift = 2 * 60 * 60; // two hours

/** The maximum allowed number of signature check operations in a block (network rule) */
static const uint64_t MAX_BLOCK_SIGOPS = MAX_BLOCK_SIZE / 50;
/** Coinbase transaction outputs can only be spent after this number of new blocks (network rule) */
static const int COINBASE_MATURITY = 30;

/** Flags for nSequence and nLockTime locks */
enum
{
    /* Interpret sequence numbers as relative lock-time constraints. */
    LOCKTIME_VERIFY_SEQUENCE = (1 << 0),

    /* Use GetMedianTimePast() instead of nTime for end point timestamp. */
    LOCKTIME_MEDIAN_TIME_PAST = (1 << 1),
};

/** Fork activation times */

// May 18th, 2015
static const int32_t VERIFY_LOW_S_FORK_HEIGHT = 687322;

// May 5th, 2018 at 00:00:00 UTC
static const int64_t LONGER_BLOCKTIME_HARDFORK = 1525478400;

// March 22, 2022 12:00:00 UTC
static const int64_t MARCH2022_ACTIVATION_TIME = 1647950400;

#endif // BITCOIN_CONSENSUS_CONSENSUS_H
