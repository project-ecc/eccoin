// This file is part of the Eccoin project
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#if defined(HAVE_CONFIG_H)
#include "config/bitcoin-config.h"
#endif

#include "tinyformat.h"
#include "util/utiltime.h"

#include <atomic>
#include <chrono>
#include <thread>

static std::atomic<int64_t> nMockTime(0); //! For unit testing

int64_t GetTime()
{
    int64_t mocktime = nMockTime.load(std::memory_order_relaxed);
    if (mocktime)
    {
        return mocktime;
    }
    time_t now = time(NULL);
    assert(now > 0);
    return now;
}

void SetMockTime(int64_t nMockTimeIn)
{
    nMockTime.store(nMockTimeIn, std::memory_order_relaxed);
}

int64_t GetMockTime()
{
    return nMockTime.load();
}

int64_t GetTimeMillis()
{
    int64_t mocktime = nMockTime.load(std::memory_order_relaxed);
    if (mocktime)
    {
        return mocktime * 1000;
    }
    std::chrono::time_point<std::chrono::system_clock> clock_now = std::chrono::system_clock::now();
    int64_t now = std::chrono::duration_cast<std::chrono::milliseconds>(clock_now.time_since_epoch()).count();
    assert(now > 0);
    return now;
}

int64_t GetTimeMicros()
{
    int64_t mocktime = nMockTime.load(std::memory_order_relaxed);
    if (mocktime)
    {
        return mocktime * 1000000;
    }
    std::chrono::time_point<std::chrono::system_clock> clock_now = std::chrono::system_clock::now();
    int64_t now = std::chrono::duration_cast<std::chrono::microseconds>(clock_now.time_since_epoch()).count();
    assert(now > 0);
    return now;
}

int64_t GetSystemTimeInSeconds()
{
    return GetTimeMicros() / 1000000;
}

/** Return a time useful for the debug log */
int64_t GetLogTimeMicros()
{
    return GetTimeMicros();
}

void MilliSleep(int64_t n)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(n));
}

std::string FormatISO8601DateTime(int64_t nTime)
{
    struct tm ts;
    time_t time_val = nTime;
#ifdef HAVE_GMTIME_R
    if (gmtime_r(&time_val, &ts) == nullptr)
    {
#else
    if (gmtime_s(&ts, &time_val) != 0)
    {
#endif
        return {};
    }
    return strprintf("%04i-%02i-%02i %02i:%02i:%02i", ts.tm_year + 1900, ts.tm_mon + 1, ts.tm_mday, ts.tm_hour,
        ts.tm_min, ts.tm_sec);
}

std::string FormatISO8601Date(int64_t nTime)
{
    struct tm ts;
    time_t time_val = nTime;
#ifdef HAVE_GMTIME_R
    if (gmtime_r(&time_val, &ts) == nullptr)
    {
#else
    if (gmtime_s(&ts, &time_val) != 0)
    {
#endif
        return {};
    }
    return strprintf("%04i-%02i-%02i", ts.tm_year + 1900, ts.tm_mon + 1, ts.tm_mday);
}
