// Copyright (c) 2019 Greg Griffith
// Copyright (c) 2019 The Eccoin Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_DATAPACKET_H
#define ROAM_DATAPACKET_H

#include "serialize.h"
#include "uint256.h"
#include "util/utiltime.h"

#include <inttypes.h>
#include <random>
#include <vector>

const uint64_t MEGABYTE = 1000000;
// BEGIN Roam Consensus Variales
const uint64_t PACKET_HEADER_SIZE = 45; // 45 bytes
const uint64_t MAX_DATA_SEGMENT_SIZE = 1 * MEGABYTE;
const uint64_t MAX_SEGMENTS_PER_PACKET = 2;
const uint64_t MAX_PACKET_SIZE = PACKET_HEADER_SIZE + (MAX_SEGMENTS_PER_PACKET * MAX_DATA_SEGMENT_SIZE);
// END
const uint8_t PACKET_VERSION = 1;

class CPacketHeader
{
public:
    uint8_t nPacketVersion; // versioning of the CPacketHeader and related classes
    uint16_t nProtocolId; // protocolId, should match the protocolid in mapBuffers
    uint64_t nTotalLength; // packet serialize size
    uint16_t nIdenfitication; // randomly generated
    uint256 nChecksum; // sha256 checksum

    CPacketHeader()
    {
        SetNull();
    }
    CPacketHeader(uint16_t nProtocolIdIn)
    {
        SetNull();
        nProtocolId = nProtocolIdIn;
        GenerateNewIdentifier();
    }

    ADD_SERIALIZE_METHODS
    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        READWRITE(nPacketVersion);
        READWRITE(nProtocolId);
        READWRITE(nTotalLength);
        READWRITE(nIdenfitication);
        READWRITE(nChecksum);
    }

    void SetNull();
    void GenerateNewIdentifier();
};

class CPacket : public CPacketHeader
{
    /// Data members
    // TODO : make these private
public:
    std::vector<uint8_t> vSender; // pubkey of sender
    std::vector<uint8_t> vSignature; // sig of the sender
    std::vector<uint8_t> vData;

    /// Methods
private:
    CPacket() : CPacketHeader() {}

public:
    CPacket(const CPacketHeader &header) : CPacketHeader(header)
    {
        *((CPacketHeader *)this) = header;
    }
    CPacket(uint16_t nProtocolIdIn) : CPacketHeader(nProtocolIdIn)
    {
        Clear();
    }
    ADD_SERIALIZE_METHODS
    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        READWRITE(*(CPacketHeader *)this);
        READWRITE(vSender);
        READWRITE(vSignature);
        READWRITE(vData);
    }

    void UpdateTotalLength();
    void AddSender(const std::vector<uint8_t> &sig);
    void AddSignature(const std::vector<uint8_t> &sig);
    void PushBackData(const std::vector<uint8_t> &data);
    std::vector<uint8_t> GetData();
    void Clear();
    CPacketHeader GetHeader();
    bool VerifySignature();
};

#endif
