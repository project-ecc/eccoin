// Copyright (c) 2019 Greg Griffith
// Copyright (c) 2019 The Eccoin Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_PACKETMANAGER_H
#define ROAM_PACKETMANAGER_H

#include <map>
#include <utility>
#include <vector>

#include "aodv.h"
#include "datapacket.h"
#include "net/net.h"
#include "pubkey.h"
#include "util/utiltime.h"
#include "validationinterface.h"

static const uint8_t DEFAULT_TIMEOUT_CHECK_INTERVAL = 3; // 3 seconds

static const uint8_t DEFAULT_PACKET_TIMEOUT = 30; // 30 seconds
static const uint8_t DEFAULT_BUFFER_TIMEOUT = 60; // 60 seconds

class PacketBuffer
{
public:
    // vRecievedPackets should be partially stored on disk at some point
    std::vector<CPacket> vRecievedPackets;
    // the protocol id using this buffer
    uint16_t nProtocolId;
    // the token needed for authentication to read vRecievedPackets
    // TODO : use a different token method because this one is very expensive to use often
    CKey boundKey;
    CPubKey boundPubkey;
    // used in the request buffer method for authentication
    uint64_t requestCount;
    uint64_t lastGoodBufferTime;
    // rejects unsigned packets if set to true, set when buffer is registered,
    // cannot be changed unless buffer is released and reregistered
    bool rejectUnsigned;

public:
    PacketBuffer()
    {
        FreeBuffer();
    }

    bool IsUsed()
    {
        return (boundKey.IsValid() == true && boundPubkey.IsValid() == true);
    }

    void FreeBuffer()
    {
        vRecievedPackets.clear();
        nProtocolId = 0;
        boundKey = CKey();
        boundPubkey = CPubKey("");
        requestCount = 0;
        lastGoodBufferTime = (uint64_t)GetTime();
        rejectUnsigned = false;
    }
};


// TODO : implement a mutex to prevent data races
class CPacketManager
{
    // Data members
private:
    // protocolId : Buffer
    std::vector<PacketBuffer> vBuffers;

    std::atomic<bool> interruptManager;
    thread_group managerThreads;

public:


    // Methods
private:
    // disallow copies
    CPacketManager(const CPacketManager &pman) = delete;
    bool BindBuffer(uint16_t protocolId, CKey &_key, CPubKey &_pubkey, bool reject);
    bool UnbindBuffer(const uint16_t &protocolId);

public:
    CPacketManager() : managerThreads(&interruptManager)
    {
        vBuffers.clear();
        // not memory efficient, but instant access is instant
        vBuffers = std::vector<PacketBuffer>(std::numeric_limits<uint16_t>::max(), PacketBuffer());
        interruptManager.store(false);
    }

    bool Start(std::string &strNodeError);

    void Interrupt();

    bool ProcessPacket(CPacket &newHeader);

    void CheckForBufferTimeouts();

    bool SendPacket(const std::vector<uint8_t> &vPubKey, const uint16_t &nProtocolId, const std::vector<uint8_t> &vData);
    bool SendPacket(const std::vector<uint8_t> &vPubKey, const uint16_t &nProtocolId, const std::vector<uint8_t> &vData, const std::string &strSenderPubKey, const std::vector<uint8_t> &vSignature);

    bool RegisterBuffer(uint16_t &protocolId, std::string &pubkey, bool reject = false);

    bool GetBuffer(uint16_t &protocolId, std::vector<CPacket> &bufferData, const std::string &sig);

    bool ReleaseBuffer(const uint16_t &protocolId, const std::string &sig);

    bool GetBufferKey(const CPubKey &pubkey, CKey &key);

    bool ResetBufferTimeout(uint16_t &protocolId, const std::string &sig);
};

extern CPacketManager g_packetman;

#endif
