// Copyright (c) 2019 Greg Griffith
// Copyright (c) 2019 The Eccoin Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "packetmanager.h"
#include "util/logger.h"

////////////////////////
///
///  Private
///

bool CPacketManager::BindBuffer(uint16_t protocolId, CKey &_key, CPubKey &_pubkey, bool reject)
{
    // disallow binding buffer 0 when not on regtest
    if (protocolId == 0 && Params().NetworkIDString() != "REGTEST")
    {
        LogPrint("roam", "CPacketManager::BindBuffer(): ERROR, protocol 0 is only allowed on regtest \n");
        return false;
    }
    if (_key.VerifyPubKey(_pubkey) == false)
    {
        LogPrint("roam", "CPacketManager::BindBuffer(): ERROR, failed to verify pubkey \n");
        return false;
    }
    if (vBuffers[protocolId].IsUsed())
    {
        LogPrint("roam", "CPacketManager::BindBuffer(): ERROR, buffer %u is already in use \n", protocolId);
        return false;
    }
    vBuffers[protocolId].FreeBuffer();
    vBuffers[protocolId].nProtocolId = protocolId;
    vBuffers[protocolId].boundKey = _key;
    vBuffers[protocolId].boundPubkey = _pubkey;
    vBuffers[protocolId].requestCount = 0;
    vBuffers[protocolId].rejectUnsigned = reject;
    return true;
}

bool CPacketManager::UnbindBuffer(const uint16_t &protocolId)
{
    if (vBuffers[protocolId].IsUsed() == false)
    {
        return true;
    }
    vBuffers[protocolId].FreeBuffer();
    return true;
}


////////////////////////
///
///  Public
///

bool CPacketManager::Start(std::string &strNodeError)
{
    managerThreads.create_thread(&CPacketManager::CheckForBufferTimeouts, this);
    return true;
}

void CPacketManager::Interrupt()
{
    interruptManager.store(true);
}

bool CPacketManager::ProcessPacket(CPacket &newPacket)
{
    // check buffer is active before checking the packet
    if (vBuffers[newPacket.nProtocolId].IsUsed() == false)
    {
        // protocolId needs to be bound by BindBuffer
        LogPrint("roam", "CPacketManager::ProcessPacket(): ERROR, buffer %u is not in use \n", newPacket.nProtocolId);
        return false;
    }
    if (vBuffers[newPacket.nProtocolId].rejectUnsigned == true && newPacket.vSignature.empty())
    {
        LogPrint("roam", "CPacketManager::ProcessPacket(): received unsigned packet on buffer that requires signature \n");
        return false;
    }
    if (newPacket.VerifySignature() == false)
    {
        LogPrint("roam", "CPacketManager::ProcessPacket(): ERROR, failed to verify packet signature \n");
        return false;
    }
    vBuffers[newPacket.nProtocolId].vRecievedPackets.push_back(std::move(newPacket));
    GetMainSignals().PacketComplete(newPacket.nProtocolId);
    return true;
}

void CPacketManager::CheckForBufferTimeouts()
{
    while (interruptManager.load() == false)
    {
        uint64_t now = (uint64_t)GetTime();
        uint64_t n = 0;
        for (PacketBuffer &buffer : vBuffers)
        {
            if (buffer.IsUsed() && buffer.lastGoodBufferTime + 60 < now)
            {
                LogPrint("roam", "CPacketManager::CheckForBufferTimeouts(): buffer %u has timed out \n", n);
                buffer.FreeBuffer();
            }
            ++n;
        }
        MilliSleep(2000); // check every other second
    }
}

bool CPacketManager::SendPacket(const std::vector<uint8_t> &vPubKey, const uint16_t &nProtocolId, const std::vector<uint8_t> &vData)
{
    // disallow sending packets on protocol 0 when not on regtest
    if (nProtocolId == 0 && Params().NetworkIDString() != "REGTEST")
    {
        LogPrint("roam", "CPacketManager::SendPacket(): ERROR, protocol 0 is only allowed on regtest \n");
        return false;
    }
    CPacket newPacket(nProtocolId);
    newPacket.PushBackData(vData);
    CPubKey searchKey(vPubKey);
    {
        LOCK(g_connman->cs_tagstore);
        CNetTagStore *tagstore = g_connman->tagstore;
        if (tagstore->HaveTag(searchKey.GetID()))
        {
            LogPrint("roam", "CPacketManager::SendPacket(): Sending packet to local buffer \n");
            return ProcessPacket(newPacket);
        }
    }
    NodeId peerNode;
    if (!g_aodvtable.GetKeyNode(vPubKey, peerNode))
    {
        LogPrint("roam", "CPacketManager::SendPacket(): ERROR, failed to get node id for provided key \n");
        return false;
    }
    g_connman->PushMessageToId(peerNode, NetMsgType::SENDPACKET, searchKey, newPacket);
    return true;
}

bool CPacketManager::SendPacket(const std::vector<uint8_t> &vPubKey, const uint16_t &nProtocolId, const std::vector<uint8_t> &vData,
    const std::string &strSenderPubKey, const std::vector<uint8_t> &vSignature)
{
    // disallow sending packets on protocol 0 when not on regtest
    if (nProtocolId == 0 && Params().NetworkIDString() != "REGTEST")
    {
        LogPrint("roam", "CPacketManager::SendPacket(): ERROR, protocol 0 is only allowed on regtest \n");
        return false;
    }
    CPacket newPacket(nProtocolId);
    newPacket.PushBackData(vData);
    newPacket.AddSender(CPubKey(strSenderPubKey).Raw());
    newPacket.AddSignature(vSignature);
    if (newPacket.VerifySignature() == false)
    {
        LogPrint("roam", "CPacketManager::SendPacket(): ERROR, failed to verify packet signature \n");
        return false;
    }
    CPubKey searchKey(vPubKey);
    {
        LOCK(g_connman->cs_tagstore);
        CNetTagStore *tagstore = g_connman->tagstore;
        if (tagstore->HaveTag(searchKey.GetID()))
        {
            LogPrint("roam", "CPacketManager::SendPacket(): Sending packet to local buffer \n");
            return ProcessPacket(newPacket);
        }
    }
    NodeId peerNode;
    if (!g_aodvtable.GetKeyNode(vPubKey, peerNode))
    {
        LogPrint("roam", "CPacketManager::SendPacket(): ERROR, failed to get node id for provided key \n");
        return false;
    }
    g_connman->PushMessageToId(peerNode, NetMsgType::SENDPACKET, searchKey, newPacket);
    return true;
}

bool CPacketManager::RegisterBuffer(uint16_t &protocolId, std::string &pubkey, bool reject)
{
    if (vBuffers[protocolId].IsUsed())
    {
        // TODO : return an error object instead of just fales to provide more information
        LogPrint("roam", "CPacketManager::RegisterBuffer(): ERROR, buffer %u is already in use \n", protocolId);
        return false;
    }
    // they new key
    CKey secret;
    secret.MakeNewKey(false);
    CPubKey _pubkey = secret.GetPubKey();
    if (BindBuffer(protocolId, secret, _pubkey, reject))
    {
        pubkey = _pubkey.Raw64Encoded();
        return true;
    }
    LogPrint("roam", "CPacketManager::RegisterBuffer(): ERROR, failed to bind buffer %u \n", protocolId);
    return false;
}

bool CPacketManager::ReleaseBuffer(const uint16_t &protocolId, const std::string &sig)
{
    if (vBuffers[protocolId].IsUsed() == false)
    {
        // the call did nothing, the buffer was not registered. the end state is the same as
        // the intended end state of the user so return true.
        return true;
    }
    PacketBuffer buffer = vBuffers[protocolId];
    bool fInvalid = false;
    std::vector<unsigned char> vchSig = DecodeBase64(sig.c_str(), &fInvalid);
    if (fInvalid)
    {
        LogPrint("roam", "CPacketManager::ReleaseBuffer(): ERROR, invalid signature \n");
        return false;
    }
    CHashWriter ss(SER_GETHASH, 0);
    std::string requestMessage = "ReleaseBufferRequest";
    ss << requestMessage;
    CPubKey pubkey;
    if (!pubkey.RecoverCompact(ss.GetHash(), vchSig))
    {
        LogPrint("roam", "CPacketManager::ReleaseBuffer(): ERROR, failed to recover pubkey \n");
        return false;
    }
    if (pubkey.GetID() != buffer.boundPubkey.GetID())
    {
        LogPrint("roam", "CPacketManager::ReleaseBuffer(): ERROR, pubkey does not match buffers registered pubkey \n");
        return false;
    }
    return UnbindBuffer(protocolId);
}

bool CPacketManager::GetBuffer(uint16_t &protocolId, std::vector<CPacket> &bufferData, const std::string &sig)
{
    if (vBuffers[protocolId].IsUsed())
    {
        PacketBuffer buffer = vBuffers[protocolId];
        bool fInvalid = false;
        std::vector<unsigned char> vchSig = DecodeBase64(sig.c_str(), &fInvalid);
        if (fInvalid)
        {
            LogPrint("roam", "CPacketManager::GetBuffer(): ERROR, invalid signature \n");
            return false;
        }
        CHashWriter ss(SER_GETHASH, 0);
        std::string requestMessage = "GetBufferRequest:";
        requestMessage += std::to_string(protocolId) + std::to_string(buffer.requestCount + 1);
        ss << requestMessage;
        CPubKey pubkey;
        if (!pubkey.RecoverCompact(ss.GetHash(), vchSig))
        {
            LogPrint("roam", "CPacketManager::GetBuffer(): ERROR, failed to recover pubkey \n");
            return false;
        }
        if (pubkey.GetID() != buffer.boundPubkey.GetID())
        {
            LogPrint("roam", "CPacketManager::GetBuffer(): ERROR, pubkey does not match buffers registered pubkey \n");
            return false;
        }
        bufferData = buffer.vRecievedPackets;
        vBuffers[protocolId].vRecievedPackets.clear();
        vBuffers[protocolId].requestCount = vBuffers[protocolId].requestCount + 1;
        vBuffers[protocolId].lastGoodBufferTime = GetTime();
        return true;
    }
    return false;
}

bool CPacketManager::GetBufferKey(const CPubKey &pubkey, CKey &key)
{
    for (auto& buffer : vBuffers)
    {
        // TODO: do a reverse mapping of pubkeys to used buffers so we dont have to search
        // like this
        if (buffer.boundPubkey.GetID() == pubkey.GetID())
        {
            key = buffer.boundKey;
            return true;
        }
    }
    LogPrint("roam", "CPacketManager::GetBufferKey(): ERROR, no key found for provided pubkey \n");
    return false;
}

bool CPacketManager::ResetBufferTimeout(uint16_t &protocolId, const std::string &sig)
{
    if (vBuffers[protocolId].IsUsed())
    {
        PacketBuffer buffer = vBuffers[protocolId];
        bool fInvalid = false;
        std::vector<unsigned char> vchSig = DecodeBase64(sig.c_str(), &fInvalid);
        if (fInvalid)
        {
            LogPrint("roam", "CPacketManager::ResetBufferTimeout(): ERROR, invalid signature \n");
            return false;
        }
        CHashWriter ss(SER_GETHASH, 0);
        std::string requestMessage = "ResetBufferTimeout";
        ss << requestMessage;
        CPubKey pubkey;
        if (!pubkey.RecoverCompact(ss.GetHash(), vchSig))
        {
            LogPrint("roam", "CPacketManager::ResetBufferTimeout(): ERROR, failed to recover pubkey \n");
            return false;
        }
        if (pubkey.GetID() != buffer.boundPubkey.GetID())
        {
            LogPrint("roam", "CPacketManager::ResetBufferTimeout(): ERROR, pubkey does not match buffers registered pubkey \n");
            return false;
        }
        vBuffers[protocolId].lastGoodBufferTime = GetTime();
        return true;
    }
    LogPrint("roam", "CPacketManager::ResetBufferTimeout(): ERROR, cannot reset buffer timeout on unused buffer \n");
    return false;
}
