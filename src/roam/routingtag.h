// This file is part of the Eccoin project
// Copyright (c) 2019 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ROAM_ROUTINGTAG_H
#define ROAM_ROUTINGTAG_H

#include "base58.h"
#include "key.h"
#include "pubkey.h"

/// CRoutingTag is its own class instead of a pair holding isPrivate and a CKey
/// or something similar to allow the addition of more data in the future if
/// needed without needing to restructure a lot of the code around it.

/// CRoutingTag does not extend CKey so that it may not be passed into a function that takes a ckey
class CRoutingTag
{
private:
    bool isPrivate;
    // tags are not compressed
    CPubKey vchPubKey;
    CPrivKey vchPrivKey;

public:
    CRoutingTag()
    {
        isPrivate = true;
    }
    CRoutingTag(bool _isPrivate, CPubKey _vchPubKey, CPrivKey _vchPrivKey)
    {
        isPrivate = _isPrivate;
        vchPubKey = _vchPubKey;
        vchPrivKey = _vchPrivKey;
    }

    ADD_SERIALIZE_METHODS

    template <typename Stream, typename Operation>
    inline void SerializationOp(Stream &s, Operation ser_action)
    {
        READWRITE(isPrivate);
        READWRITE(vchPubKey);
        READWRITE(vchPrivKey);
    }

    CPubKey GetPubKey() const;
    CPrivKey GetPrivKey() const;
    bool IsPrivate() const;
    void ConvertToPublicTag();
    void MakeNewKey();
    bool VerifyPubKey(const CPubKey &pubkey) const;
    bool CheckIfValid() const;
    bool Sign(const uint256 &hash, std::vector<unsigned char> &vchSig, uint32_t test_case = 0) const;
    bool SignCompact(const uint256 &hash, std::vector<unsigned char> &vchSig) const;
};

/**
 * A base58-encoded secret key
 */
class CTagSecret : public CBase58Data
{
public:
    void SetKey(const CPrivKey &privkey)
    {
        CKey key;
        // compressed is false. tags are not compressed.
        key.SetPrivKey(privkey, false);
        SetKey(key);
    }
    void SetKey(const CKey &vchSecret)
    {
        assert(vchSecret.IsValid());
        SetData(Params().Base58Prefix(CChainParams::SECRET_KEY), vchSecret.begin(), vchSecret.size());
        if (vchSecret.IsCompressed())
        {
            vchData.push_back(1);
        }
    }
    CKey GetKey()
    {
        CKey ret;
        assert(vchData.size() >= 32);
        ret.Set(vchData.begin(), vchData.begin() + 32, vchData.size() > 32 && vchData[32] == 1);
        return ret;
    }
    bool IsValid() const
    {
        bool fExpectedFormat = vchData.size() == 32 || (vchData.size() == 33 && vchData[32] == 1);
        bool fCorrectVersion = vchVersion == Params().Base58Prefix(CChainParams::SECRET_KEY);
        return fExpectedFormat && fCorrectVersion;
    }
    bool SetString(const char *pszSecret)
    {
        return CBase58Data::SetString(pszSecret) && IsValid();
    }
    bool SetString(const std::string &strSecret)
    {
        return SetString(strSecret.c_str());
    }
    CTagSecret(const CKey &vchSecret)
    {
        SetKey(vchSecret);
    }
    CTagSecret(const CPrivKey &vchSecret)
    {
        SetKey(vchSecret);
    }
    CTagSecret() {}
};

#endif // ECCOIN_ROUTINGTAG_H
