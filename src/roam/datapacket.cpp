// Copyright (c) 2019 Greg Griffith
// Copyright (c) 2019 The Eccoin Developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "datapacket.h"
#include "pubkey.h"
#include "util/logger.h"

void CPacketHeader::SetNull()
{
    nPacketVersion = PACKET_VERSION;
    nTotalLength = PACKET_HEADER_SIZE;
    nIdenfitication = 0;
    nProtocolId = 0;
    nChecksum.SetNull();
}

void CPacketHeader::GenerateNewIdentifier()
{
    uint64_t seed = GetTime();
    std::mt19937_64 rand(seed); // Standard mersenne_twister_engine seeded with rd()
    nIdenfitication = rand() % std::numeric_limits<uint16_t>::max();
}

void CPacket::UpdateTotalLength()
{
    nTotalLength = PACKET_HEADER_SIZE;
    nTotalLength += GetSizeOfCompactSize(vSender.size()) + vSender.size();
    nTotalLength += GetSizeOfCompactSize(vSignature.size()) + vSignature.size();
    nTotalLength += GetSizeOfCompactSize(vData.size()) + vData.size();
}

void CPacket::AddSender(const std::vector<uint8_t> &sender)
{
    vSender = sender;
    UpdateTotalLength();
}

void CPacket::AddSignature(const std::vector<uint8_t> &sig)
{
    vSignature = sig;
    UpdateTotalLength();
}

void CPacket::PushBackData(const std::vector<uint8_t> &data)
{
    vData.insert(vData.end(), data.begin(), data.end());
    UpdateTotalLength();
}

void CPacket::Clear()
{
    vSender.clear();
    vSignature.clear();
    vData.clear();
    UpdateTotalLength();
}

std::vector<uint8_t> CPacket::GetData()
{
    return vData;
}
CPacketHeader CPacket::GetHeader()
{
    CPacketHeader header;
    header.nPacketVersion = this->nPacketVersion;
    header.nProtocolId = this->nProtocolId;
    header.nTotalLength = this->nTotalLength;
    header.nIdenfitication = this->nIdenfitication;
    header.nChecksum = this->nChecksum;
    return header;
}

bool CPacket::VerifySignature()
{
    if (vSender.empty() && vSignature.empty())
    {
        // No signature nor sender is valid
        return true;
    }

    CHashWriter ss(SER_GETHASH, 0);
    ss << vData;

    CPubKey sender(vSender);
    CPubKey pubkey;
    if (!pubkey.RecoverCompact(ss.GetHash(), vSignature))
    {
        LogPrint("roam", "CPacket::VerifySignature(): ERROR, failed to recover compact pubkey \n");
        return false;
    }

    return (pubkey.GetID() == sender.GetID());
}
