// This file is part of the Eccoin project
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "validation/processheader.h"

#include "init.h"
#include "main.h"
#include "timedata.h"
#include "util/util.h"
#include "validation/forks.h"


bool AcceptBlockHeader(const CBlockHeader &block,
    CValidationState &state,
    const CChainParams &chainparams,
    CBlockIndex **ppindex)
{
    AssertLockHeld(cs_main);
    // AssertRecursiveWriteLockHeld(g_chain.cs_mapBlockIndex);
    // Check for duplicate
    uint256 hash = block.GetHash();
    CBlockIndex *pindex = nullptr;
    if (hash != chainparams.GetConsensus().hashGenesisBlock)
    {
        pindex = g_chain.LookupBlockIndex(hash);
        if (pindex)
        {
            // Block header is already known.
            if (ppindex)
            {
                *ppindex = pindex;
            }
            if (pindex->nStatus & BLOCK_FAILED_MASK)
            {
                return state.DoS(10, error("%s: block is marked invalid", __func__), REJECT_INVALID, "duplicate");
            }
            return true;
        }

        if (!CheckBlockHeader(block, state))
            return false;

        // Get prev block index
        CBlockIndex *pindexPrev = g_chain.LookupBlockIndex(block.hashPrevBlock);
        if (!pindexPrev)
        {
            return state.DoS(10, error("%s: prev block not found", __func__), 0, "bad-prevblk");
        }
        if (pindexPrev->nStatus & BLOCK_FAILED_MASK)
            return state.DoS(100, error("%s: prev block invalid", __func__), REJECT_INVALID, "bad-prevblk");

        assert(pindexPrev);
        if (fCheckpointsEnabled && !CheckIndexAgainstCheckpoint(pindexPrev, state, chainparams, hash))
            return error("%s: CheckIndexAgainstCheckpoint(): %s", __func__, state.GetRejectReason().c_str());

        if (!ContextualCheckBlockHeader(block, state, pindexPrev))
            return false;
    }
    if (pindex == nullptr)
    {
        pindex = g_chain.AddToBlockIndex(block);
    }

    if (ppindex)
    {
        *ppindex = pindex;
    }

    return true;
}


bool CheckBlockHeader(const CBlockHeader &block, CValidationState &state)
{
    // Check timestamp
    if (block.GetBlockTime() > GetAdjustedTime() + 2 * 60 * 60)
        return state.Invalid(
            error("CheckBlockHeader(): block timestamp too far in the future"), REJECT_INVALID, "time-too-new");

    return true;
}


bool ContextualCheckBlockHeader(const CBlockHeader &block, CValidationState &state, CBlockIndex *const pindexPrev)
{
    const int nHeight = pindexPrev == nullptr ? 0 : pindexPrev->nHeight + 1;
    if (IsMarch2022Enabled(Params().GetConsensus(), pindexPrev))
    {
        // Check proof of stake
        uint32_t expectedNbits = GetNextTargetRequired(pindexPrev, true);
        if (block.nBits != expectedNbits)
        {
            return state.DoS(100,
                error("%s: incorrect required work. Height %d, Block nBits 0x%x, expected 0x%x", __func__, nHeight,
                    block.nBits, expectedNbits),
                REJECT_INVALID, "bad-diffbits");
        }
    }

    if (fCheckpointsEnabled)
    {
        const CChainParams &chainparams = Params();
        // If the parent block belongs to the set of checkpointed blocks but it has a mismatched hash,
        // then we are on the wrong fork so ignore
        if (!CheckAgainstCheckpoint(pindexPrev->nHeight, *pindexPrev->phashBlock, chainparams))
        {
            return error("%s: CheckAgainstCheckpoint(): %s", __func__, state.GetRejectReason().c_str());
        }

        const CBlockIndex *pindexMainChain = g_chain[nHeight];
        if (pindexMainChain && pindexMainChain->GetBlockHeader() == block)
        {
            return true; // if this header is in the main chain it is valid and we are done
        }
        // Don't accept any forks from the main chain prior to the last checkpoint.
        CBlockIndex *pindex = Checkpoints::GetLastCheckpoint(chainparams.Checkpoints());
        if (pindex && nHeight < pindex->nHeight)
        {
            return state.DoS(100,
                error("%s: forked chain is older than last checkpoint (height %d)", __func__, nHeight),
                REJECT_CHECKPOINT, "bad-fork-prior-to-checkpoint");
        }
    }

    // Check timestamp against prev
    if (block.GetBlockTime() <= pindexPrev->GetMedianTimePast())
    {
        return state.Invalid(error("%s: block's timestamp is too early", __func__), REJECT_INVALID, "time-too-old");
    }

    return true;
}
