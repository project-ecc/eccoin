// This file is part of the Eccoin project
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include <atomic>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/foreach.hpp>
#include <boost/math/distributions/poisson.hpp>
#include <boost/range/adaptor/reversed.hpp>

#include <sstream>

#include "args.h"
#include "blockstorage/blockstorage.h"
#include "chain/checkpoints.h"
#include "checkqueue.h"
#include "consensus/merkle.h"
#include "consensus/tx_verify.h"
#include "crypto/hash.h"
#include "init.h"
#include "kernel.h"
#include "main.h"
#include "net/messages.h"
#include "net/net.h"
#include "net/requestmanager.h"
#include "policy/policy.h"
#include "txmempool.h"
#include "undo.h"
#include "util/util.h"
#include "validation/forks.h"
#include "validation/processblock.h"
#include "validation/processheader.h"
#include "validationinterface.h"


std::atomic<bool> fLargeWorkForkFound{false};
std::atomic<bool> fLargeWorkInvalidChainFound{false};
CBlockIndex *pindexBestForkTip = nullptr;
CBlockIndex *pindexBestForkBase = nullptr;

/** Update g_chain and related internal data structures. */
void UpdateTip(CBlockIndex *pindexNew)
{
    g_chain.SetTip(pindexNew);

    // If the chain tip has changed previously rejected transactions
    // might be now valid, e.g. due to a nLockTime'd tx becoming valid,
    // or a double-spend. Reset the rejects filter and give those
    // txs a second chance.
    recentRejects->reset();

    // New best block
    nTimeBestReceived.store(GetTime());
    mempool.AddTransactionsUpdated(1);

    LogPrintf("%s: new best=%s  height=%d  log2_work=%.8g  tx=%lu  date=%s cache=%.1fMiB(%utx)\n", __func__,
        g_chain.Tip()->GetBlockHash().ToString(), g_chain.Height(),
        log(g_chain.Tip()->nChainWork.getdouble()) / log(2.0), (unsigned long)(g_chain.Tip()->nChainTx),
        FormatISO8601DateTime(g_chain.Tip()->GetBlockTime()), pcoinsTip->DynamicMemoryUsage() * (1.0 / (1 << 20)),
        pcoinsTip->GetCacheSize());

    cvBlockChange.notify_all();
}

/** Disconnect g_chain's tip. You probably want to call mempool.removeForReorg and manually re-limit mempool size
 * after this, with cs_main held. */
bool DisconnectTip(CValidationState &state, const Consensus::Params &consensusParams)
{
    CBlockIndex *pindexDelete = g_chain.Tip();
    assert(pindexDelete);
    // Read block from disk.
    std::shared_ptr<CBlock> pblock = std::make_shared<CBlock>();
    CBlock &block = *pblock;
    {
        if (!ReadBlockFromDisk(block, pindexDelete, consensusParams))
        {
            return AbortNode(state, "Failed to read block");
        }
    }
    // Apply the block atomically to the chain state.
    {
        CCoinsViewCache view(pcoinsTip.get());
        if (DisconnectBlock(block, pindexDelete, view) != DISCONNECT_OK)
        {
            return error("DisconnectTip(): DisconnectBlock %s failed", pindexDelete->GetBlockHash().ToString());
        }
        assert(view.Flush());
    }
    // Write the chain state to disk, if necessary.
    if (!FlushStateToDisk(state, FLUSH_STATE_IF_NEEDED))
    {
        return false;
    }

    // clear recentRejects to allow already processed txs to be processed again due to chain rollback
    recentRejects->reset();

    // Update g_chain and related variables.
    UpdateTip(pindexDelete->pprev);
    // Let wallets know transactions went from 1-confirmed to
    // 0-confirmed or conflicted:
    for (const auto &ptx : block.vtx)
    {
        SyncWithWallets(ptx, nullptr, -1);
    }

    // clear the mempool and resubmit transactions
    // TODO : this is probably not performant but it works for now
    {
        std::vector<CTransactionRef> mempoolTxs = mempool.GetAllTxs();
        mempool.clear();
        for (auto const &ptx : block.vtx)
        {
            // coinbase and coinstake txs are never added to the mempool.
            if (ptx->IsCoinBase() || ptx->IsCoinStake())
            {
                continue;
            }
            CValidationState stateDummy;
            AcceptToMemoryPool(mempool, stateDummy, ptx, false, NULL, true);
        }
        for (auto &ptx : mempoolTxs)
        {
            CValidationState stateDummy;
            AcceptToMemoryPool(mempool, stateDummy, ptx, false, NULL, true);
        }
    }
    return true;
}

/**
 * Connect a new block to g_chain. pblock is either NULL or a pointer to a CBlock
 * corresponding to pindexNew, to bypass loading it again from disk.
 */
bool ConnectTip(CValidationState &state, const CChainParams &chainparams, CBlockIndex *pindexNew, const CBlock *pblock)
{
    AssertLockHeld(cs_main);
    assert(pindexNew->pprev == g_chain.Tip());
    // Read block from disk.
    CBlock block;
    if (!pblock)
    {
        if (!ReadBlockFromDisk(block, pindexNew, chainparams.GetConsensus()))
        {
            return AbortNode(state, "Failed to read block");
        }
        pblock = &block;
    }

    // Apply the block atomically to the chain state.
    {
        CCoinsViewCache view(pcoinsTip.get());
        bool rv = ConnectBlock(*pblock, state, pindexNew, view);
        if (!rv)
        {
            if (state.IsInvalid())
            {
                InvalidBlockFound(pindexNew, state);
            }
            return error("ConnectTip(): ConnectBlock %s failed", pindexNew->GetBlockHash().ToString().c_str());
        }
        assert(view.Flush());
    }
    // Write the chain state to disk, if necessary.
    if (!FlushStateToDisk(state, FLUSH_STATE_IF_NEEDED))
        return false;
    // Remove conflicting transactions from the mempool.
    std::list<CTransactionRef> txConflicted;
    mempool.removeForBlock(pblock->vtx, pindexNew->nHeight, txConflicted, !g_chain.IsInitialBlockDownload());
    // Update g_chain & related variables.
    UpdateTip(pindexNew);

    // Tell wallet about transactions that went from mempool
    // to conflicted:
    for (const auto &ptx : txConflicted)
    {
        SyncWithWallets(ptx, nullptr, -1);
    }
    // ... and about transactions that got confirmed:
    int txIdx = 0;
    for (const auto &ptx : pblock->vtx)
    {
        SyncWithWallets(ptx, pblock, txIdx);
        txIdx++;
    }
    return true;
}

// Execute a command, as given by -alertnotify, on certain events such as a long fork being seen
void AlertNotify(const std::string &strMessage)
{
    std::string strCmd = gArgs.GetArg("-alertnotify", "");
    if (strCmd.empty())
        return;

    // Alert text should be plain ascii coming from a trusted source, but to
    // be safe we first strip anything not in safeChars, then add single quotes around
    // the whole string before passing it to the shell:
    std::string singleQuote("'");
    std::string safeStatus = SanitizeString(strMessage);
    safeStatus = singleQuote + safeStatus + singleQuote;
    boost::replace_all(strCmd, "%s", safeStatus);

    boost::thread t(runCommand, strCmd); // thread runs free
}

void CheckForkWarningConditions()
{
    AssertLockHeld(cs_main);
    // Before we get past initial download, we cannot reliably alert about forks
    // (we assume we don't get stuck on a fork before the last checkpoint)
    if (g_chain.IsInitialBlockDownload())
        return;

    // If our best fork is no longer within 72 blocks (+/- 12 hours if no one mines it)
    // of our head, drop it
    if (pindexBestForkTip && g_chain.Height() - pindexBestForkTip->nHeight >= 72)
        pindexBestForkTip = NULL;

    if (pindexBestForkTip || (pindexBestInvalid && pindexBestInvalid.load()->nChainWork >
                                                       g_chain.Tip()->nChainWork + (GetBlockProof(*g_chain.Tip()) * 6)))
    {
        if (!fLargeWorkForkFound && pindexBestForkBase)
        {
            std::string warning = std::string("'Warning: Large-work fork detected, forking after block ") +
                                  pindexBestForkBase->phashBlock->ToString() + std::string("'");
            AlertNotify(warning);
        }
        if (pindexBestForkTip && pindexBestForkBase)
        {
            LogPrintf("%s: Warning: Large valid fork found\n  forking the chain at height %d (%s)\n  lasting to height "
                      "%d (%s).\nChain state database corruption likely.\n",
                __func__, pindexBestForkBase->nHeight, pindexBestForkBase->phashBlock->ToString(),
                pindexBestForkTip->nHeight, pindexBestForkTip->phashBlock->ToString());
            fLargeWorkForkFound = true;
        }
        else
        {
            LogPrintf("%s: Warning: Found invalid chain at least ~6 blocks longer than our best chain.\nChain state "
                      "database corruption likely.\n",
                __func__);
            fLargeWorkInvalidChainFound = true;
        }
    }
    else
    {
        fLargeWorkForkFound = false;
        fLargeWorkInvalidChainFound = false;
    }
}

void CheckForkWarningConditionsOnNewFork(CBlockIndex *pindexNewForkTip)
{
    AssertLockHeld(cs_main);
    // If we are on a fork that is sufficiently large, set a warning flag
    CBlockIndex *pfork = pindexNewForkTip;
    CBlockIndex *plonger = g_chain.Tip();
    while (pfork && pfork != plonger)
    {
        while (plonger && plonger->nHeight > pfork->nHeight)
            plonger = plonger->pprev;
        if (pfork == plonger)
            break;
        pfork = pfork->pprev;
    }

    // We define a condition where we should warn the user about as a fork of at least 7 blocks
    // with a tip within 72 blocks (+/- 12 hours if no one mines it) of ours
    // We use 7 blocks rather arbitrarily as it represents just under 10% of sustained network
    // hash rate operating on the fork.
    // or a chain that is entirely longer than ours and invalid (note that this should be detected by both)
    // We define it this way because it allows us to only store the highest fork tip (+ base) which meets
    // the 7-block condition and from this always have the most-likely-to-cause-warning fork
    if (pfork &&
        (!pindexBestForkTip || (pindexBestForkTip && pindexNewForkTip->nHeight > pindexBestForkTip->nHeight)) &&
        pindexNewForkTip->nChainWork - pfork->nChainWork > (GetBlockProof(*pfork) * 7) &&
        g_chain.Height() - pindexNewForkTip->nHeight < 72)
    {
        pindexBestForkTip = pindexNewForkTip;
        pindexBestForkBase = pfork;
    }

    CheckForkWarningConditions();
}

/**
 * Try to make some progress towards making pindexMostWork the active block.
 * pblock is either NULL or a pointer to a CBlock corresponding to pindexMostWork.
 */
bool ActivateBestChainStep(CValidationState &state,
    const CChainParams &chainparams,
    CBlockIndex *pindexMostWork,
    const CBlock *pblock)
{
    AssertLockHeld(cs_main);
    bool fInvalidFound = false;
    const CBlockIndex *pindexOldTip = g_chain.Tip();
    const CBlockIndex *pindexFork = g_chain.FindFork(pindexMostWork);

    // Disconnect active blocks which are no longer in the best chain.
    bool fBlocksDisconnected = false;
    while (g_chain.Tip() && g_chain.Tip() != pindexFork)
    {
        if (!DisconnectTip(state, chainparams.GetConsensus()))
            return false;
        fBlocksDisconnected = true;
    }

    // Build list of new blocks to connect.
    std::vector<CBlockIndex *> vpindexToConnect;
    bool fContinue = true;
    int nHeight = pindexFork ? pindexFork->nHeight : -1;
    bool fBlock = true;
    while (fContinue && nHeight < pindexMostWork->nHeight)
    {
        // Don't iterate the entire list of potential improvements toward the best tip, as we likely only need
        // a few blocks along the way.
        int nTargetHeight = std::min(nHeight + 32, pindexMostWork->nHeight);
        vpindexToConnect.clear();
        vpindexToConnect.reserve(nTargetHeight - nHeight);
        CBlockIndex *pindexIter = pindexMostWork->GetAncestor(nTargetHeight);
        while (pindexIter && pindexIter->nHeight != nHeight)
        {
            vpindexToConnect.push_back(pindexIter);
            pindexIter = pindexIter->pprev;
        }
        nHeight = nTargetHeight;

        // Connect new blocks.
        CBlockIndex *pindexNewTip = nullptr;
        for (auto i = vpindexToConnect.rbegin(); i != vpindexToConnect.rend(); i++)
        {
            CBlockIndex *pindexConnect = *i;
            if (!ConnectTip(
                    state, chainparams, pindexConnect, pindexConnect == pindexMostWork && fBlock ? pblock : nullptr))
            {
                if (state.IsInvalid())
                {
                    // The block violates a consensus rule.
                    if (!state.CorruptionPossible())
                        InvalidChainFound(vpindexToConnect.back());
                    fInvalidFound = true;
                    fContinue = false;
                    break;
                }
                else
                {
                    // A system error occurred (disk space, database error, ...).
                    return false;
                }
            }
            else
            {
                pindexNewTip = pindexConnect;
                if (!g_chain.IsInitialBlockDownload())
                {
                    // Notify external zmq listeners about the new tip.
                    GetMainSignals().UpdatedBlockTip(pindexConnect);
                }
                BlockNotifyCallback(g_chain.IsInitialBlockDownload(), pindexNewTip);

                PruneBlockIndexCandidates();
                if (!pindexOldTip || g_chain.Tip()->nChainWork > pindexOldTip->nChainWork)
                {
                    // We're in a better position than we were. Return temporarily to release the lock.
                    fContinue = false;
                    break;
                }
            }
        }
        if (fInvalidFound)
            break; // stop processing more blocks if the last one was invalid.

        if (fContinue)
        {
            pindexMostWork = FindMostWorkChain();
            if (!pindexMostWork)
                return false;
        }
        fBlock = false; // read next blocks from disk
    }

    // Relay Inventory
    CBlockIndex *pindexNewTip = g_chain.Tip();
    if (pindexFork != pindexNewTip)
    {
        if (!g_chain.IsInitialBlockDownload())
        {
            // Find the hashes of all blocks that weren't previously in the best chain.
            std::vector<uint256> vHashes;
            CBlockIndex *pindexToAnnounce = pindexNewTip;
            while (pindexToAnnounce != pindexFork)
            {
                vHashes.push_back(pindexToAnnounce->GetBlockHash());
                pindexToAnnounce = pindexToAnnounce->pprev;
                if (vHashes.size() == MAX_BLOCKS_TO_ANNOUNCE)
                {
                    // Limit announcements in case of a huge reorganization.
                    // Rely on the peer's synchronization mechanism in that case.
                    break;
                }
            }

            // Relay inventory, but don't relay old inventory during initial block download.
            const int nNewHeight = pindexNewTip->nHeight;
            g_connman->ForEachNode([nNewHeight, &vHashes](CNode *pnode) {
                if (nNewHeight > (pnode->nStartingHeight != -1 ? pnode->nStartingHeight - 2000 : 0))
                {
                    for (const uint256 &hash : boost::adaptors::reverse(vHashes))
                    {
                        pnode->PushBlockHash(hash);
                    }
                }
            });
        }
    }

    if (fBlocksDisconnected)
    {
        mempool.removeForReorg(pcoinsTip.get(), g_chain.Tip()->nHeight + 1, STANDARD_LOCKTIME_VERIFY_FLAGS);
        LimitMempoolSize(mempool, gArgs.GetArg("-maxmempool", DEFAULT_MAX_MEMPOOL_SIZE) * 1000000,
            gArgs.GetArg("-mempoolexpiry", DEFAULT_MEMPOOL_EXPIRY) * 60 * 60);
    }
    mempool.check(pcoinsTip.get());
    // Callbacks/notifications for a new best chain.
    if (fInvalidFound)
    {
        CheckForkWarningConditionsOnNewFork(vpindexToConnect.back());
        return false;
    }
    else
    {
        CheckForkWarningConditions();
    }
    return true;
}

/**
 * Make the best chain active, in multiple steps. The result is either failure
 * or an activated best chain. pblock is either NULL or a pointer to a block
 * that is already loaded (to avoid loading it again from disk).
 */
bool ActivateBestChain(CValidationState &state, const CChainParams &chainparams, const CBlock *pblock)
{
    CBlockIndex *pindexMostWork = nullptr;
    LOCK(cs_main);

    do
    {
        if (shutdown_threads.load())
        {
            break;
        }

        pindexMostWork = FindMostWorkChain();
        if (!pindexMostWork)
        {
            return true;
        }

        // Whether we have anything to do at all.
        if (g_chain.Tip() != nullptr)
        {
            if (pindexMostWork->nChainWork <= g_chain.Tip()->nChainWork)
                return true;
        }
        if (!ActivateBestChainStep(state, chainparams, pindexMostWork,
                pblock && pblock->GetHash() == pindexMostWork->GetBlockHash() ? pblock : nullptr))
        {
            return false;
        }
        pindexMostWork = FindMostWorkChain();
        if (!pindexMostWork)
            return false;
        pblock = nullptr;
    } while (pindexMostWork->nChainWork > g_chain.Tip()->nChainWork);
    CheckBlockIndex(chainparams.GetConsensus());
    // Write changes periodically to disk
    if (!FlushStateToDisk(state, FLUSH_STATE_PERIODIC))
    {
        return false;
    }
    return true;
}


void CheckBlockIndex(const Consensus::Params &consensusParams)
{
    if (!fCheckBlockIndex)
    {
        return;
    }

    LOCK(cs_main);
    RECURSIVEREADLOCK(g_chain.cs_mapBlockIndex);

    // During a reindex, we read the genesis block and call CheckBlockIndex before ActivateBestChain,
    // so we have the genesis block in mapBlockIndex but no active chain.  (A few of the tests when
    // iterating the block tree require that g_chain has been initialized.)
    if (g_chain.Height() < 0)
    {
        assert(g_chain.mapBlockIndex.size() <= 1);
        return;
    }

    // Build forward-pointing map of the entire block tree.
    std::multimap<CBlockIndex *, CBlockIndex *> forward;
    for (BlockMap::iterator it = g_chain.mapBlockIndex.begin(); it != g_chain.mapBlockIndex.end(); it++)
    {
        forward.insert(std::make_pair(it->second->pprev, it->second));
    }

    assert(forward.size() == g_chain.mapBlockIndex.size());

    std::pair<std::multimap<CBlockIndex *, CBlockIndex *>::iterator,
        std::multimap<CBlockIndex *, CBlockIndex *>::iterator>
        rangeGenesis = forward.equal_range(NULL);
    CBlockIndex *pindex = rangeGenesis.first->second;
    rangeGenesis.first++;
    assert(rangeGenesis.first == rangeGenesis.second); // There is only one index entry with parent NULL.

    // Iterate over the entire block tree, using depth-first search.
    // Along the way, remember whether there are blocks on the path from genesis
    // block being explored which are the first to have certain properties.
    size_t nNodes = 0;
    int nHeight = 0;
    CBlockIndex *pindexFirstInvalid = NULL; // Oldest ancestor of pindex which is invalid.
    CBlockIndex *pindexFirstMissing = NULL; // Oldest ancestor of pindex which does not have BLOCK_HAVE_DATA.
    CBlockIndex *pindexFirstNeverProcessed = NULL; // Oldest ancestor of pindex for which nTx == 0.
    // Oldest ancestor of pindex which does not have BLOCK_VALID_TREE (regardless of being valid or not).
    CBlockIndex *pindexFirstNotTreeValid = NULL;
    // Oldest ancestor of pindex which does not have BLOCK_VALID_TRANSACTIONS (regardless of being valid or not).
    CBlockIndex *pindexFirstNotTransactionsValid = NULL;
    // Oldest ancestor of pindex which does not have BLOCK_VALID_CHAIN (regardless of being valid or not).
    CBlockIndex *pindexFirstNotChainValid = NULL;
    // Oldest ancestor of pindex which does not have BLOCK_VALID_SCRIPTS (regardless of being valid or not).
    CBlockIndex *pindexFirstNotScriptsValid = NULL;
    while (pindex != NULL)
    {
        nNodes++;
        if (pindexFirstInvalid == NULL && pindex->nStatus & BLOCK_FAILED_VALID)
            pindexFirstInvalid = pindex;
        if (pindexFirstMissing == NULL && !(pindex->nStatus & BLOCK_HAVE_DATA))
            pindexFirstMissing = pindex;
        if (pindexFirstNeverProcessed == NULL && pindex->nTx == 0)
            pindexFirstNeverProcessed = pindex;
        if (pindex->pprev != NULL && pindexFirstNotTreeValid == NULL &&
            (pindex->nStatus & BLOCK_VALID_MASK) < BLOCK_VALID_TREE)
            pindexFirstNotTreeValid = pindex;
        if (pindex->pprev != NULL && pindexFirstNotTransactionsValid == NULL &&
            (pindex->nStatus & BLOCK_VALID_MASK) < BLOCK_VALID_TRANSACTIONS)
            pindexFirstNotTransactionsValid = pindex;
        if (pindex->pprev != NULL && pindexFirstNotChainValid == NULL &&
            (pindex->nStatus & BLOCK_VALID_MASK) < BLOCK_VALID_CHAIN)
            pindexFirstNotChainValid = pindex;
        if (pindex->pprev != NULL && pindexFirstNotScriptsValid == NULL &&
            (pindex->nStatus & BLOCK_VALID_MASK) < BLOCK_VALID_SCRIPTS)
            pindexFirstNotScriptsValid = pindex;

        // Begin: actual consistency checks.
        if (pindex->pprev == NULL)
        {
            // Genesis block checks.
            assert(pindex->GetBlockHash() == consensusParams.hashGenesisBlock); // Genesis block's hash must match.
            // The current active chain's genesis block must be this block.
            assert(pindex == g_chain.Genesis());
        }
        // nSequenceId can't be set for blocks that aren't linked
        if (pindex->nChainTx == 0)
            assert(pindex->nSequenceId == 0);
        // VALID_TRANSACTIONS is equivalent to nTx > 0 for all nodes (whether or not pruning has occurred).
        // HAVE_DATA is only equivalent to nTx > 0 (or VALID_TRANSACTIONS) if no pruning has occurred.

        // If we've never pruned, then HAVE_DATA should be equivalent to nTx > 0
        assert(!(pindex->nStatus & BLOCK_HAVE_DATA) == (pindex->nTx == 0));
        assert(pindexFirstMissing == pindexFirstNeverProcessed);

        if (pindex->nStatus & BLOCK_HAVE_UNDO)
            assert(pindex->nStatus & BLOCK_HAVE_DATA);
        // This is pruning-independent.
        assert(((pindex->nStatus & BLOCK_VALID_MASK) >= BLOCK_VALID_TRANSACTIONS) == (pindex->nTx > 0));
        // All parents having had data (at some point) is equivalent to all parents being VALID_TRANSACTIONS, which is
        // equivalent to nChainTx being set.
        // nChainTx != 0 is used to signal that all parent blocks have been processed (but may have been pruned).
        assert((pindexFirstNeverProcessed != NULL) == (pindex->nChainTx == 0));
        assert((pindexFirstNotTransactionsValid != NULL) == (pindex->nChainTx == 0));
        assert(pindex->nHeight == nHeight); // nHeight must be consistent.
        // For every block except the genesis block, the chainwork must be larger than the parent's.
        assert(pindex->pprev == NULL || pindex->nChainWork >= pindex->pprev->nChainWork);
        // The pskip pointer must point back for all but the first 2 blocks.
        assert(nHeight < 2 || (pindex->pskip && (pindex->pskip->nHeight < nHeight)));
        assert(pindexFirstNotTreeValid == NULL); // All mapBlockIndex entries must at least be TREE valid
        // TREE valid implies all parents are TREE valid
        if ((pindex->nStatus & BLOCK_VALID_MASK) >= BLOCK_VALID_TREE)
            assert(pindexFirstNotTreeValid == NULL);
        // CHAIN valid implies all parents are CHAIN valid
        if ((pindex->nStatus & BLOCK_VALID_MASK) >= BLOCK_VALID_CHAIN)
            assert(pindexFirstNotChainValid == NULL);
        // SCRIPTS valid implies all parents are SCRIPTS valid
        if ((pindex->nStatus & BLOCK_VALID_MASK) >= BLOCK_VALID_SCRIPTS)
            assert(pindexFirstNotScriptsValid == NULL);
        if (pindexFirstInvalid == NULL)
        {
            // Checks for not-invalid blocks.
            // The failed mask cannot be set for blocks without invalid parents.
            assert((pindex->nStatus & BLOCK_FAILED_MASK) == 0);
        }
        if (!CBlockIndexWorkComparator()(pindex, g_chain.Tip()) && pindexFirstNeverProcessed == NULL)
        {
            if (pindexFirstInvalid == NULL)
            {
                // If this block sorts at least as good as the current tip and
                // is valid and we have all data for its parents, it must be in
                // setBlockIndexCandidates.  g_chain.Tip() must also be there
                // even if some data has been pruned.
                if (pindexFirstMissing == NULL || pindex == g_chain.Tip())
                {
                    assert(setBlockIndexCandidates.count(pindex));
                }
                // If some parent is missing, then it could be that this block was in
                // setBlockIndexCandidates but had to be removed because of the missing data.
                // In this case it must be in mapBlocksUnlinked -- see test below.
            }
            // If this block sorts worse than the current tip or some ancestor's block has never been seen, it cannot be
            // in setBlockIndexCandidates.
        }
        else
        {
            assert(setBlockIndexCandidates.count(pindex) == 0);
        }
        // Check whether this block is in mapBlocksUnlinked.
        std::pair<std::multimap<CBlockIndex *, CBlockIndex *>::iterator,
            std::multimap<CBlockIndex *, CBlockIndex *>::iterator>
            rangeUnlinked = mapBlocksUnlinked.equal_range(pindex->pprev);
        bool foundInUnlinked = false;
        while (rangeUnlinked.first != rangeUnlinked.second)
        {
            assert(rangeUnlinked.first->first == pindex->pprev);
            if (rangeUnlinked.first->second == pindex)
            {
                foundInUnlinked = true;
                break;
            }
            rangeUnlinked.first++;
        }
        if (pindex->pprev && (pindex->nStatus & BLOCK_HAVE_DATA) && pindexFirstNeverProcessed != NULL &&
            pindexFirstInvalid == NULL)
        {
            // If this block has block data available, some parent was never received, and has no invalid parents, it
            // must be in mapBlocksUnlinked.
            assert(foundInUnlinked);
        }
        // Can't be in mapBlocksUnlinked if we don't HAVE_DATA
        if (!(pindex->nStatus & BLOCK_HAVE_DATA))
            assert(!foundInUnlinked);
        // We aren't missing data for any parent -- cannot be in mapBlocksUnlinked.
        if (pindexFirstMissing == NULL)
            assert(!foundInUnlinked);
        if (pindex->pprev && (pindex->nStatus & BLOCK_HAVE_DATA) && pindexFirstNeverProcessed == NULL &&
            pindexFirstMissing != NULL)
        {
            // We HAVE_DATA for this block, have received data for all parents at some point, but we're currently
            // missing data for some parent.
            assert(false); // We must have pruned but pruning was removed so something just went wrong.
        }
        // assert(pindex->GetBlockHash() == pindex->GetBlockHeader().GetHash()); // Perhaps too slow
        // End: actual consistency checks.

        // Try descending into the first subnode.
        std::pair<std::multimap<CBlockIndex *, CBlockIndex *>::iterator,
            std::multimap<CBlockIndex *, CBlockIndex *>::iterator>
            range = forward.equal_range(pindex);
        if (range.first != range.second)
        {
            // A subnode was found.
            pindex = range.first->second;
            nHeight++;
            continue;
        }
        // This is a leaf node.
        // Move upwards until we reach a node of which we have not yet visited the last child.
        while (pindex)
        {
            // We are going to either move to a parent or a sibling of pindex.
            // If pindex was the first with a certain property, unset the corresponding variable.
            if (pindex == pindexFirstInvalid)
                pindexFirstInvalid = NULL;
            if (pindex == pindexFirstMissing)
                pindexFirstMissing = NULL;
            if (pindex == pindexFirstNeverProcessed)
                pindexFirstNeverProcessed = NULL;
            if (pindex == pindexFirstNotTreeValid)
                pindexFirstNotTreeValid = NULL;
            if (pindex == pindexFirstNotTransactionsValid)
                pindexFirstNotTransactionsValid = NULL;
            if (pindex == pindexFirstNotChainValid)
                pindexFirstNotChainValid = NULL;
            if (pindex == pindexFirstNotScriptsValid)
                pindexFirstNotScriptsValid = NULL;
            // Find our parent.
            CBlockIndex *pindexPar = pindex->pprev;
            // Find which child we just visited.
            std::pair<std::multimap<CBlockIndex *, CBlockIndex *>::iterator,
                std::multimap<CBlockIndex *, CBlockIndex *>::iterator>
                rangePar = forward.equal_range(pindexPar);
            while (rangePar.first->second != pindex)
            {
                // Our parent must have at least the node we're coming from as child.
                assert(rangePar.first != rangePar.second);
                rangePar.first++;
            }
            // Proceed to the next one.
            rangePar.first++;
            if (rangePar.first != rangePar.second)
            {
                // Move to the sibling.
                pindex = rangePar.first->second;
                break;
            }
            else
            {
                // Move up further.
                pindex = pindexPar;
                nHeight--;
                continue;
            }
        }
    }

    // Check that we actually traversed the entire map.
    assert(nNodes == forward.size());
}


/**
 * Return the tip of the chain with the most work in it, that isn't
 * known to be invalid (it's however far from certain to be valid).
 */
CBlockIndex *FindMostWorkChain()
{
    do
    {
        CBlockIndex *pindexNew = NULL;

        // Find the best candidate header.
        {
            std::set<CBlockIndex *, CBlockIndexWorkComparator>::reverse_iterator it = setBlockIndexCandidates.rbegin();
            if (it == setBlockIndexCandidates.rend())
                return NULL;
            pindexNew = *it;
        }

        // Check whether all blocks on the path between the currently active chain and the candidate are valid.
        // Just going until the active chain is an optimization, as we know all blocks in it are valid already.
        CBlockIndex *pindexTest = pindexNew;
        bool fInvalidAncestor = false;
        while (pindexTest && !g_chain.Contains(pindexTest))
        {
            assert(pindexTest->nChainTx || pindexTest->nHeight == 0);

            // Pruned nodes may have entries in setBlockIndexCandidates for
            // which block files have been deleted.  Remove those as candidates
            // for the most work chain if we come across them; we can't switch
            // to a chain unless we have all the non-active-chain parent blocks.
            bool fFailedChain = pindexTest->nStatus & BLOCK_FAILED_MASK;
            bool fMissingData = !(pindexTest->nStatus & BLOCK_HAVE_DATA);
            if (fFailedChain || fMissingData)
            {
                // Candidate chain is not usable (either invalid or missing data)
                if (fFailedChain &&
                    (pindexBestInvalid == nullptr || pindexNew->nChainWork > pindexBestInvalid.load()->nChainWork))
                {
                    pindexBestInvalid = pindexNew;
                }
                CBlockIndex *pindexFailed = pindexNew;
                // Remove the entire chain from the set.
                while (pindexTest != pindexFailed)
                {
                    if (fFailedChain)
                    {
                        pindexFailed->nStatus |= BLOCK_FAILED_CHILD;
                    }
                    else if (fMissingData)
                    {
                        // If we're missing data, then add back to mapBlocksUnlinked,
                        // so that if the block arrives in the future we can try adding
                        // to setBlockIndexCandidates again.
                        mapBlocksUnlinked.insert(std::make_pair(pindexFailed->pprev, pindexFailed));
                    }
                    setBlockIndexCandidates.erase(pindexFailed);
                    pindexFailed = pindexFailed->pprev;
                }
                setBlockIndexCandidates.erase(pindexTest);
                fInvalidAncestor = true;
                break;
            }
            pindexTest = pindexTest->pprev;
        }
        if (!fInvalidAncestor)
        {
            return pindexNew;
        }
    } while (true);
}

void InvalidChainFound(CBlockIndex *pindexNew)
{
    if (!pindexBestInvalid || pindexNew->nChainWork > pindexBestInvalid.load()->nChainWork)
    {
        pindexBestInvalid = pindexNew;
    }

    LogPrintf("%s: invalid block=%s  height=%d  log2_work=%.8g  date=%s\n", __func__,
        pindexNew->GetBlockHash().ToString(), pindexNew->nHeight, log(pindexNew->nChainWork.getdouble()) / log(2.0),
        FormatISO8601DateTime(pindexNew->GetBlockTime()));
    CBlockIndex *tip = g_chain.Tip();
    assert(tip);
    LogPrintf("%s:  current best=%s  height=%d  log2_work=%.8g  date=%s\n", __func__, tip->GetBlockHash().ToString(),
        g_chain.Height(), log(tip->nChainWork.getdouble()) / log(2.0), FormatISO8601DateTime(tip->GetBlockTime()));
    LOCK(cs_main);
    CheckForkWarningConditions();
}

void InvalidBlockFound(CBlockIndex *pindex, const CValidationState &state)
{
    if (!state.CorruptionPossible())
    {
        pindex->nStatus |= BLOCK_FAILED_VALID;
        setDirtyBlockIndex.insert(pindex);
        setBlockIndexCandidates.erase(pindex);
        InvalidChainFound(pindex);
    }
}


bool FindUndoPos(CValidationState &state, int nFile, CDiskBlockPos &pos, unsigned int nAddSize)
{
    pos.nFile = nFile;

    LOCK(cs_LastBlockFile);

    unsigned int nNewSize;
    pos.nPos = vinfoBlockFile[nFile].nUndoSize;
    nNewSize = vinfoBlockFile[nFile].nUndoSize += nAddSize;
    setDirtyFileInfo.insert(nFile);

    unsigned int nOldChunks = (pos.nPos + UNDOFILE_CHUNK_SIZE - 1) / UNDOFILE_CHUNK_SIZE;
    unsigned int nNewChunks = (nNewSize + UNDOFILE_CHUNK_SIZE - 1) / UNDOFILE_CHUNK_SIZE;
    if (nNewChunks > nOldChunks)
    {
        if (CheckDiskSpace(nNewChunks * UNDOFILE_CHUNK_SIZE - pos.nPos))
        {
            FILE *file = OpenUndoFile(pos);
            if (file)
            {
                LogPrintf(
                    "Pre-allocating up to position 0x%x in rev%05u.dat\n", nNewChunks * UNDOFILE_CHUNK_SIZE, pos.nFile);
                AllocateFileRange(file, pos.nPos, nNewChunks * UNDOFILE_CHUNK_SIZE - pos.nPos);
                fclose(file);
            }
        }
        else
            return state.Error("out of disk space");
    }

    return true;
}

bool CheckCoinStakeReward(const CBlock &block, CValidationState &state, const int64_t &nValueIn)
{
    const Consensus::Params &consensusParams = Params().GetConsensus();
    const CBlockIndex *pindexPrev = g_chain.Tip();
    const CTransaction &tx = *block.vtx[1];

    if (IsMarch2022Enabled(consensusParams, pindexPrev))
    {
        const int64_t nBlockReward = tx.GetValueOut() - nValueIn;
        if (nBlockReward > GetProofOfStakeReward(nValueIn))
        {
            LogPrint("kernel", "nStakeReward = %d > %d, \n", nBlockReward, GetProofOfStakeReward(nValueIn));
            return state.DoS(100, false, REJECT_INVALID, "bad-txns-stake-reward-too-high", false,
                strprintf("CheckCoinStakeReward(): %s input-calculated stake reward exceeded",
                    tx.GetHash().ToString().substr(0, 10).c_str()));
        }
    }
    else
    {
        const int64_t nStakeReward = tx.GetValueOut() - nValueIn;
        // coin stake tx earns reward instead of paying fee
        uint64_t nCoinAge;
        if (!GetCoinAge(tx, block.nTime, nCoinAge))
        {
            return state.DoS(100, false, REJECT_INVALID, "bad-txns-cant-get-coin-age", false,
                strprintf("ConnectInputs() : %s unable to get coin age for coinstake",
                    tx.GetHash().ToString().substr(0, 10).c_str()));
        }
        if (nStakeReward > GetProofOfStakeReward(nCoinAge, 0))
        {
            LogPrint("kernel", "nStakeReward = %d , CoinAge = %d \n", nStakeReward, nCoinAge);
            return state.DoS(100, false, REJECT_INVALID, "bad-txns-stake-reward-too-high", false,
                strprintf("CheckCoinStakeReward(): %s age-calculated stake reward exceeded",
                    tx.GetHash().ToString().substr(0, 10).c_str()));
        }
    }
    return true;
}

static CCheckQueue<CScriptCheck> scriptcheckqueue(128);

void InterruptScriptCheck()
{
    scriptcheckqueue.Interrupt();
}
void ThreadScriptCheck()
{
    RenameThread("eccoind-scriptch");
    scriptcheckqueue.Thread();
}

bool ConnectBlock(const CBlock &block,
    CValidationState &state,
    CBlockIndex *pindex,
    CCoinsViewCache &view,
    bool fJustCheck)
{
    const CChainParams &chainparams = Params();
    AssertLockHeld(cs_main);

    /// due to a bug move PoW block rejection check to after height 1504000
    if (block.IsProofOfWork() && pindex->nHeight >= 1504000)
    {
        return state.DoS(100, error("ConnectBlock(): proof of work failed, invalid PoW height "), REJECT_INVALID,
            "Pow after cutoff");
    }

    if (pindex->GetBlockHash() != chainparams.GetConsensus().hashGenesisBlock)
    {
        // need cs_mapBlockIndex to update the index
        RECURSIVEWRITELOCK(g_chain.cs_mapBlockIndex);
        // once updateForPos runs the only flags that should be enabled are the ones that determine if PoS block or not
        // before this runs there should have been no flags set. so it is ok to reset the flags to 0
        pindex->updateForPos(block);
    }

    // Check it again in case a previous version let a bad block in
    if (!CheckBlock(block, state, !fJustCheck, !fJustCheck))
        return false;

    // verify that the view's current state corresponds to the previous block
    uint256 hashPrevBlock = pindex->pprev == nullptr ? uint256() : pindex->pprev->GetBlockHash();
    assert(hashPrevBlock == view.GetBestBlock());

    // Special case for the genesis block, skipping connection of its transactions
    // (its coinbase is unspendable)
    if (block.GetHash() == chainparams.GetConsensus().hashGenesisBlock)
    {
        if (!fJustCheck)
            view.SetBestBlock(pindex->GetBlockHash());
        return true;
    }

    bool fScriptChecks = true;
    if (fCheckpointsEnabled)
    {
        CBlockIndex *pindexLastCheckpoint = Checkpoints::GetLastCheckpoint(chainparams.Checkpoints());
        if (pindexLastCheckpoint && pindexLastCheckpoint->GetAncestor(pindex->nHeight) == pindex)
        {
            // This block is an ancestor of a checkpoint: disable script checks
            fScriptChecks = false;
        }
    }

    for (auto const &tx : block.vtx)
    {
        for (size_t o = 0; o < tx->vout.size(); o++)
        {
            if (view.HaveCoin(COutPoint(tx->GetHash(), o)))
            {
                return state.DoS(
                    100, error("ConnectBlock(): tried to overwrite transaction"), REJECT_INVALID, "bad-txns-BIP30");
            }
        }
    }

    uint32_t flags = SCRIPT_VERIFY_P2SH;
    flags |= SCRIPT_VERIFY_DERSIG;
    flags |= SCRIPT_VERIFY_CHECKLOCKTIMEVERIFY;
    flags |= SCRIPT_VERIFY_CHECKSEQUENCEVERIFY;

    uint32_t nLockTimeFlags = LOCKTIME_VERIFY_SEQUENCE;

    if (pindex->nHeight >= VERIFY_LOW_S_FORK_HEIGHT)
    {
        flags |= SCRIPT_VERIFY_LOW_S;
    }

    if (IsMarch2022Enabled(chainparams.GetConsensus(), pindex->pprev))
    {
        // schnorr
        flags |= MANDATORY_BLOCK_SCRIPT_FLAGS;
        flags |= SCRIPT_ENABLE_SCHNORR;
    }

    CBlockUndo blockundo;

    CCheckQueueControl<CScriptCheck> control(fScriptChecks && nScriptCheckThreads ? &scriptcheckqueue : nullptr);

    std::vector<int> prevheights;
    CAmount nFees = 0;
    CAmount nCoinstakeValueIn = 0;
    CAmount nValueIn = 0;
    CAmount nValueOut = 0;
    int nInputs = 0;
    unsigned int nSigOps = 0;
    CDiskTxPos pos(pindex->GetBlockPos(), GetSizeOfCompactSize(block.vtx.size()));
    std::vector<std::pair<uint256, CDiskTxPos> > vPos;
    vPos.reserve(block.vtx.size());
    blockundo.vtxundo.reserve(block.vtx.size() - 1);
    for (unsigned int i = 0; i < block.vtx.size(); i++)
    {
        const CTransaction &tx = *(block.vtx[i]);

        // version 3 or 4 of any transaction should have a time of 0
        if (tx.nVersion == 3 || tx.nVersion == 4)
        {
            if (tx.nTime != 0)
            {
                return state.DoS(
                    100, error("ConnectBlock(): invalid transaction time"), REJECT_INVALID, "bad-txn-time");
            }
        }

        nInputs += tx.vin.size();
        nSigOps += GetLegacySigOpCount(tx);
        if (nSigOps > MAX_BLOCK_SIGOPS)
            return state.DoS(100, error("ConnectBlock(): too many sigops"), REJECT_INVALID, "bad-blk-sigops");

        if (tx.IsCoinBase())
        {
            nValueOut += tx.GetValueOut();
        }
        else
        {
            // coinstake or normal txs
            if (!view.HaveInputs(tx))
            {
                return state.DoS(100, error("ConnectBlock(): inputs missing/spent"), REJECT_INVALID,
                    "bad-txns-inputs-missingorspent");
            }

            // Check that transaction is BIP68 final
            // BIP68 lock checks (as opposed to nLockTime checks) must
            // be in ConnectBlock because they require the UTXO set
            prevheights.resize(tx.vin.size());
            for (size_t j = 0; j < tx.vin.size(); j++)
            {
                prevheights[j] = CoinAccessor(view, tx.vin[j].prevout)->nHeight;
            }

            if (!SequenceLocks(tx, nLockTimeFlags, &prevheights, *pindex))
            {
                return state.DoS(100, error("%s: contains a non-BIP68-final transaction", __func__), REJECT_INVALID,
                    "bad-txns-nonfinal");
            }

            {
                // Add in sigops done by pay-to-script-hash inputs;
                // this is to prevent a "rogue miner" from creating
                // an incredibly-expensive-to-validate block.
                nSigOps += GetP2SHSigOpCount(tx, view);
                if (nSigOps > MAX_BLOCK_SIGOPS)
                {
                    return state.DoS(100, error("ConnectBlock(): too many sigops"), REJECT_INVALID, "bad-blk-sigops");
                }
            }

            const CAmount nTxValueIn = view.GetValueIn(tx);
            const CAmount nTxValueOut = tx.GetValueOut();

            if (!tx.IsCoinStake())
            {
                int64_t nTxFees = nTxValueIn - nTxValueOut;
                // if (fork())
                // {
                //     nTxFees = nTxFees / 2;
                // }
                nFees += nTxFees;
            }
            else
            {
                nCoinstakeValueIn = nTxValueIn;
            }

            nValueIn += nTxValueIn;
            nValueOut += nTxValueOut;

            std::vector<CScriptCheck> vChecks;
            bool fCacheResults = fJustCheck; /* Don't cache results if we're actually connecting blocks (still consult
                                                the cache, though) */

            // coinstake uses its own checks that are a subset of checkInputs to avoid passing the tx time into
            // checkInputs which is only needed by the coinstake. CheckInputs is used in a couple of places
            // where we do not have a tx time to submit, it is easiet to give the coinstake its own
            // subset of checks
            if (tx.IsCoinStake())
            {
                // checkCoinStake does not check that the reward paid is valid
                if (!CheckCoinStake(
                        tx, state, view, fScriptChecks, flags, fCacheResults, nScriptCheckThreads ? &vChecks : NULL))
                {
                    return error("ConnectBlock(): CheckCoinStake on %s failed with %s", tx.GetHash().ToString(),
                        FormatStateMessage(state));
                }
            }
            else
            {
                if (!CheckInputs(
                        tx, state, view, fScriptChecks, flags, fCacheResults, nScriptCheckThreads ? &vChecks : NULL))
                {
                    return error("ConnectBlock(): CheckInputs on %s failed with %s", tx.GetHash().ToString(),
                        FormatStateMessage(state));
                }
            }
            control.Add(vChecks);
        }

        CTxUndo undoDummy;
        if (i > 0)
        {
            blockundo.vtxundo.push_back(CTxUndo());
        }
        UpdateCoins(tx, view, i == 0 ? undoDummy : blockundo.vtxundo.back(), pindex->nHeight, block.nTime);
        vPos.push_back(std::make_pair(tx.GetHash(), pos));
        pos.nTxOffset += ::GetSerializeSize(tx, SER_DISK, CLIENT_VERSION);
    }

    // BEGIN check block reward

    if (block.IsProofOfWork())
    {
        const int64_t blockReward = GetProofOfWorkReward(nFees, pindex->nHeight, block.hashPrevBlock);
        /*
        if (pindex->nHeight >= 1493605 && pindex->nHeight <= 1504000)
        {
            // Intentionally skip coinbase payout amount check
            // A few blocks between 1493605 and 1504000 were mined with pow and had high coinbase payout.
            // Pow blocks should have been entirely disabled  but due to a bug where PoW was no longer
            // being checked properly
            // No funds were stolen from other people
        }
        else
        {
            if (block.vtx[0]->GetValueOut() > blockReward)
            {
                return state.DoS(100,
                    error("ConnectBlock(): coinbase pays too much (actual=%d vs limit=%d)", block.vtx[0]->GetValueOut(),
                        blockReward),
                    REJECT_INVALID, "bad-cb-amount");
            }
        }
        */
    }
    else // PoS Block
    {
        if (!CheckCoinStakeReward(block, state, nCoinstakeValueIn))
        {
            return error("ConnectBlock(): CheckCoinStakeReward failed with %s", FormatStateMessage(state));
        }
        /*
        for (const auto &tx : block.vtx)
        {
            if (tx->IsCoinStake())
            {
                uint64_t nCoinAge;
                if (!GetCoinAge(*tx, block.nTime, nCoinAge))
                    return state.DoS(100, error("ConnectBlock() : %s unable to get coin age for coinstake",
                                              tx->GetHash().ToString().substr(0, 10).c_str()));
                blockReward = blockReward + GetProofOfStakeReward(nCoinAge, pindex->nHeight);
            }
        }
        if (block.vtx[0]->GetValueOut() > blockReward && pindex->nHeight >= 1504000)
        {
            return state.DoS(100, error("ConnectBlock(): coinstake pays too much"), REJECT_INVALID, "bad-cs-amount");
        }
        */
    }
    // END check block reward

    // if we are connecting this block then the previous block is the current chain tip
    const CBlockIndex *chainTip = pindex->pprev;

    if (!control.Wait())
    {
        return state.DoS(100, error("%s: CheckQueue failed", __func__), REJECT_INVALID, "block-validation-failed");
    }

    {
        RECURSIVEWRITELOCK(g_chain.cs_mapBlockIndex);
        // ppcoin: track money supply and mint amount info
        pindex->nMint = nValueOut - nValueIn + nFees;
        pindex->nMoneySupply = (chainTip ? chainTip->nMoneySupply : 0) + nValueOut - nValueIn;
    }

    /// put the following checks in this function due to lack of pindex when checkblock is called
    // Verify hash target and signature of coinstake tx
    uint256 hashProofOfStake;
    hashProofOfStake.SetNull();
    if (block.IsProofOfStake())
    {
        if (!CheckProofOfStake(chainTip, *(block.vtx[1]), block.nTime, hashProofOfStake, flags))
        {
            return state.DoS(100,
                error("WARNING: ProcessBlock(): check proof-of-stake failed for block %s\n",
                    block.GetHash().ToString().c_str()),
                REJECT_INVALID, "bad-proofofstake");
        }
    }

    // ppcoin: compute stake entropy bit for stake modifier
    if (!pindex->SetStakeEntropyBit(block.GetStakeEntropyBit()))
    {
        return error("ConnectBlock() : SetStakeEntropyBit() failed");
    }

    {
        RECURSIVEWRITELOCK(g_chain.cs_mapBlockIndex);
        // ppcoin: record proof-of-stake hash value
        pindex->hashProofOfStake = hashProofOfStake;
    }

    // ppcoin: compute stake modifier
    uint256 nStakeModifier;
    nStakeModifier.SetNull();
    if (block.IsProofOfStake())
    {
        if (!ComputeNextStakeModifier(chainTip, *(block.vtx[1]), nStakeModifier))
            return state.DoS(100, error("ConnectBlock() : ComputeNextStakeModifier() failed"), REJECT_INVALID,
                "bad-stakemodifier-pos");
    }
    else
    {
        if (!ComputeNextStakeModifier(chainTip, *(block.vtx[0]), nStakeModifier))
            return state.DoS(100, error("ConnectBlock() : ComputeNextStakeModifier() failed"), REJECT_INVALID,
                "bad-stakemodifier-pow");
    }
    {
        RECURSIVEWRITELOCK(g_chain.cs_mapBlockIndex);
        pindex->SetStakeModifier(nStakeModifier);
    }
    if (fJustCheck)
        return true;

    // Write undo information to disk
    if (pindex->GetUndoPos().IsNull() || !pindex->IsValid(BLOCK_VALID_SCRIPTS))
    {
        if (pindex->GetUndoPos().IsNull())
        {
            CDiskBlockPos _pos;
            if (!FindUndoPos(state, pindex->nFile, _pos, ::GetSerializeSize(blockundo, SER_DISK, CLIENT_VERSION) + 40))
                return error("ConnectBlock(): FindUndoPos failed");
            {
                if (!UndoWriteToDisk(blockundo, _pos, chainTip->GetBlockHash(), chainparams.MessageStart()))
                    return AbortNode(state, "Failed to write undo data");
            }

            // update nUndoPos in block index
            pindex->nUndoPos = _pos.nPos;
            pindex->nStatus |= BLOCK_HAVE_UNDO;
        }
        pindex->RaiseValidity(BLOCK_VALID_SCRIPTS);
        setDirtyBlockIndex.insert(pindex);
    }

    if (!pblocktree->WriteTxIndex(vPos))
    {
        return AbortNode(state, "Failed to write transaction index");
    }

    // add this block to the view's block chain
    view.SetBestBlock(pindex->GetBlockHash());
    return true;
}

/**
 * Apply the undo operation of a CTxInUndo to the given chain state.
 * @param undo The Coin to be restored.
 * @param view The coins view to which to apply the changes.
 * @param out The out point that corresponds to the tx input.
 * @return True on success.
 */
int ApplyTxInUndo(Coin &&undo, CCoinsViewCache &view, const COutPoint &out)
{
    bool fClean = true;
    if (view.HaveCoin(out))
    {
        LogPrintf("Apply Undo: Unclean disconnect of (%s, %d)\n", out.hash.ToString(), out.n);
        fClean = false; // overwriting transaction output
    }
    if (undo.nHeight == 0)
    {
        // Missing undo metadata (height and coinbase). Older versions included this
        // information only in undo records for the last spend of a transactions
        // outputs. This implies that it must be present for some other output of the same tx.
        CoinAccessor alternate(view, out.hash);
        if (alternate->IsSpent())
        {
            LogPrintf("Apply Undo: Coin (%s, %d) is spent\n", out.hash.ToString(), out.n);
            return DISCONNECT_FAILED; // adding output for transaction without known metadata
        }
        undo.nHeight = alternate->nHeight;
        undo.fCoinBase = alternate->fCoinBase;
        undo.fCoinStake = alternate->fCoinStake;
        undo.nTime = alternate->nTime;
    }
    view.AddCoin(out, std::move(undo), !fClean);
    return fClean ? DISCONNECT_OK : DISCONNECT_UNCLEAN;
}

/** Undo the effects of this block (with given index) on the UTXO set represented by coins.
 *  When UNCLEAN or FAILED is returned, view is left in an indeterminate state. */
DisconnectResult DisconnectBlock(const CBlock &block, const CBlockIndex *pindex, CCoinsViewCache &view)
{
    assert(pindex->GetBlockHash() == view.GetBestBlock());

    bool fClean = true;

    CBlockUndo blockUndo;
    CDiskBlockPos pos = pindex->GetUndoPos();
    if (pos.IsNull())
    {
        error("DisconnectBlock(): no undo data available");
        return DISCONNECT_FAILED;
    }

    if (!UndoReadFromDisk(blockUndo, pos, pindex->pprev->GetBlockHash()))
    {
        error("DisconnectBlock(): failure reading undo data");
        return DISCONNECT_FAILED;
    }

    // coinstake is second transaction, first transaction should still be ignored in PoS blocks
    if (blockUndo.vtxundo.size() + 1 != block.vtx.size())
    {
        error("DisconnectBlock(): block and undo data inconsistent, vtxundo.size +1 %u != vtx.size %u",
            blockUndo.vtxundo.size() + 1, block.vtx.size());
        return DISCONNECT_FAILED;
    }
    // undo transactions in reverse of the OTI algorithm order (so add inputs first, then remove outputs)
    for (unsigned int i = 0; i < block.vtx.size(); i++)
    {
        // coinbase has no inputs, coinstake does. only skip if coinbase
        if (block.vtx[i]->IsCoinBase())
        {
            continue;
        }
        const CTransaction &tx = *(block.vtx[i]);
        CTxUndo &txundo = blockUndo.vtxundo[i - 1];
        if (txundo.vprevout.size() != tx.vin.size())
        {
            error("DisconnectBlock(): transaction and undo data inconsistent, vprevout.size %u  != vin.size %u with tx "
                  "hash %s",
                txundo.vprevout.size(), tx.vin.size(), tx.GetHash().ToString().c_str());
            return DISCONNECT_FAILED;
        }
        for (unsigned int j = tx.vin.size(); j-- > 0;)
        {
            const COutPoint &out = tx.vin[j].prevout;
            int res = ApplyTxInUndo(std::move(txundo.vprevout[j]), view, out);
            if (res == DISCONNECT_FAILED)
            {
                error("DisconnectBlock(): ApplyTxInUndo failed");
                return DISCONNECT_FAILED;
            }
            fClean = fClean && res != DISCONNECT_UNCLEAN;
        }
        // At this point, all of txundo.vprevout should have been moved out.
    }

    // remove outputs
    for (unsigned int j = 0; j < block.vtx.size(); j++)
    {
        const CTransaction &tx = *(block.vtx[j]);
        uint256 hash = tx.GetHash();

        // Check that all outputs are available and match the outputs in the block itself exactly.
        for (size_t o = 0; o < tx.vout.size(); o++)
        {
            if (!tx.vout[o].scriptPubKey.IsUnspendable())
            {
                COutPoint out(hash, o);
                Coin coin;
                bool is_spent = view.SpendCoin(out, &coin);
                if (!is_spent || tx.vout[o] != coin.out)
                {
                    error("DisconnectBlock(): transaction output mismatch");
                    error("%s != %s", tx.vout[o].ToString().c_str(), coin.out.ToString().c_str());
                    fClean = false; // transaction output mismatch
                }
            }
        }
    }

    // move best block pointer to prevout block
    view.SetBestBlock(pindex->pprev->GetBlockHash());

    return fClean ? DISCONNECT_OK : DISCONNECT_UNCLEAN;
}


/** Store block on disk. If dbp is non-NULL, the file is known to already reside on disk */
bool AcceptBlock(const CBlock *pblock,
    CValidationState &state,
    const CChainParams &chainparams,
    CBlockIndex **ppindex,
    bool fRequested,
    CDiskBlockPos *dbp)
{
    AssertLockHeld(cs_main);

    CBlockIndex *&pindex = *ppindex;

    if (!AcceptBlockHeader(*pblock, state, chainparams, &pindex))
        return false;

    // Try to process all requested blocks that we don't have, but only
    // process an unrequested block if it's new and has enough work to
    // advance our tip, and isn't too many blocks ahead.
    bool fAlreadyHave = pindex->nStatus & BLOCK_HAVE_DATA;
    bool fHasMoreWork = (g_chain.Tip() ? pindex->nChainWork > g_chain.Tip()->nChainWork : true);
    // Blocks that are too out-of-order needlessly limit the effectiveness of
    // pruning, because pruning will not delete block files that contain any
    // blocks which are too close in height to the tip.  Apply this test
    // regardless of whether pruning is enabled; it should generally be safe to
    // not process unrequested blocks.
    bool fTooFarAhead = (pindex->nHeight > int(g_chain.Height() + MIN_BLOCKS_TO_KEEP));

    // TODO: deal better with return value and error conditions for duplicate
    // and unrequested blocks.
    if (fAlreadyHave)
        return true;
    if (!fRequested)
    { // If we didn't ask for it:
        if (pindex->nTx != 0)
            return true; // This is a previously-processed block that was pruned
        if (!fHasMoreWork)
            return true; // Don't process less-work chains
        if (fTooFarAhead)
            return true; // Block height is too high
    }

    if ((!CheckBlock(*pblock, state)) || !ContextualCheckBlock(*pblock, state, pindex->pprev))
    {
        if (state.IsInvalid() && !state.CorruptionPossible())
        {
            pindex->nStatus |= BLOCK_FAILED_VALID;
            setDirtyBlockIndex.insert(pindex);
        }
        return false;
    }

    int nHeight = pindex->nHeight;

    // Write block to history file
    try
    {
        unsigned int nBlockSize = ::GetSerializeSize(*pblock, SER_DISK, CLIENT_VERSION);
        CDiskBlockPos blockPos;
        if (dbp != NULL)
            blockPos = *dbp;
        if (!FindBlockPos(state, blockPos, nBlockSize + 8, nHeight, (*pblock).GetBlockTime(), dbp != NULL))
            return error("AcceptBlock(): FindBlockPos failed");
        if (dbp == NULL)
        {
            if (!WriteBlockToDisk(*pblock, blockPos, chainparams.MessageStart()))
                AbortNode(state, "Failed to write block");
        }
        if (!ReceivedBlockTransactions(*pblock, state, pindex, blockPos))
            return error("AcceptBlock(): ReceivedBlockTransactions failed");
    }
    catch (const std::runtime_error &e)
    {
        return AbortNode(state, std::string("System error: ") + e.what());
    }

    return true;
}

bool CheckBlock(const CBlock &block, CValidationState &state, bool fCheckPOW, bool fCheckMerkleRoot)
{
    // These are checks that are independent of context.
    if (block.fChecked)
    {
        return true;
    }

    if (block.IsProofOfWork() && fCheckPOW && !CheckProofOfWork(block.GetHash(), block.nBits, Params().GetConsensus()))
    {
        return state.DoS(50, error("CheckBlockHeader(): proof of work failed"), REJECT_INVALID, "high-hash");
    }

    // Check that the header is valid (particularly PoW).  This is mostly
    // redundant with the call in AcceptBlockHeader.
    if (!CheckBlockHeader(block, state))
    {
        return error("%s: CheckBlockHeader FAILED", __func__);
    }

    // Check the merkle root.
    if (fCheckMerkleRoot)
    {
        bool mutated;
        uint256 hashMerkleRoot2 = BlockMerkleRoot(block, &mutated);
        if (block.hashMerkleRoot != hashMerkleRoot2)
        {
            return state.DoS(
                100, error("CheckBlock(): hashMerkleRoot mismatch"), REJECT_INVALID, "bad-txnmrklroot", true);
        }

        // Check for merkle tree malleability (CVE-2012-2459): repeating sequences
        // of transactions in a block without affecting the merkle root of a block,
        // while still invalidating it.
        if (mutated)
        {
            return state.DoS(
                100, error("CheckBlock(): duplicate transaction"), REJECT_INVALID, "bad-txns-duplicate", true);
        }
    }

    // All potential-corruption validation must be done before we do any
    // transaction validation, as otherwise we may mark the header as invalid
    // because we receive the wrong transactions for it.

    // Size limits
    if (block.vtx.empty() || block.vtx.size() > MAX_BLOCK_SIZE ||
        ::GetSerializeSize(block, SER_NETWORK, PROTOCOL_VERSION) > MAX_BLOCK_SIZE)
    {
        return state.DoS(100, error("CheckBlock(): size limits failed"), REJECT_INVALID, "bad-blk-length");
    }

    // First transaction must be coinbase, the rest must not be
    if (block.vtx.empty() || !block.vtx[0]->IsCoinBase())
    {
        return state.DoS(100, error("CheckBlock(): first tx is not coinbase"), REJECT_INVALID, "bad-cb-missing");
    }

    for (unsigned int i = 1; i < block.vtx.size(); i++)
    {
        if (block.vtx[i]->IsCoinBase())
        {
            return state.DoS(100, error("CheckBlock(): more than one coinbase"), REJECT_INVALID, "bad-cb-multiple");
        }
    }

    // PoS: only the second transaction can be the optional coinstake
    for (unsigned int i = 2; i < block.vtx.size(); i++)
    {
        if (block.vtx[i]->IsCoinStake())
        {
            return state.DoS(100, error("CheckBlock() : coinstake in wrong position"));
        }
    }

    // PoS: coinbase output should be empty if proof-of-stake block
    if (block.IsProofOfStake() && (block.vtx[0]->vout.size() != 1 || !block.vtx[0]->vout[0].IsEmpty()))
    {
        return state.DoS(0, error("CheckBlock() : coinbase output not empty for proof-of-stake block"));
    }

    // Check transactions
    for (auto const &tx : block.vtx)
    {
        if (!CheckTransaction(*tx, state))
        {
            return state.Invalid(false, state.GetRejectCode(), state.GetRejectReason(),
                strprintf("Transaction check failed (txid %s) %s", tx->GetId().ToString(), state.GetDebugMessage()));
        }
        // PoS: check transaction timestamp if tx version has an nTime field
        // there is no time field in version 3 and 4
        // this check can be removed when version 1 and 2 are no longer allowed
        if (tx->nVersion == 1 || tx->nVersion == 2)
        {
            if (block.GetBlockTime() < (int64_t)tx->nTime)
            {
                return state.DoS(50, error("CheckBlock() : block timestamp earlier than transaction timestamp"));
            }
        }
        // version 3 or 4 of any transaction should have a time of 0
        if (tx->nVersion == 3 || tx->nVersion == 4)
        {
            if (tx->nTime != 0)
            {
                return state.DoS(50, error("CheckBlock(): invalid version 3 or 4 transaction time"));
            }
        }
    }

    unsigned int nSigOps = 0;
    for (auto const &tx : block.vtx)
    {
        nSigOps += GetLegacySigOpCount(*tx);
    }
    if (nSigOps > MAX_BLOCK_SIGOPS)
    {
        return state.DoS(100, error("CheckBlock(): out-of-bounds SigOpCount"), REJECT_INVALID, "bad-blk-sigops");
    }

    // PoS: check block signature
    if (!block.CheckBlockSignature())
    {
        return state.DoS(100, error("CheckBlock() : bad block signature"), REJECT_INVALID, "bad-block-sig");
    }

    if (fCheckPOW && fCheckMerkleRoot)
    {
        block.fChecked = true;
    }

    return true;
}

bool ContextualCheckBlock(const CBlock &block, CValidationState &state, CBlockIndex *const pindexPrev)
{
    const int nHeight = pindexPrev == NULL ? 0 : pindexPrev->nHeight + 1;
    const Consensus::Params &consensusParams = Params().GetConsensus();

    // Start enforcing BIP113 (Median Time Past) using versionbits logic.
    int nLockTimeFlags = 0;
    nLockTimeFlags |= LOCKTIME_MEDIAN_TIME_PAST;

    int64_t nLockTimeCutoff;
    if (pindexPrev == nullptr)
    {
        nLockTimeCutoff = block.GetBlockTime();
    }
    else
    {
        nLockTimeCutoff =
            (nLockTimeFlags & LOCKTIME_MEDIAN_TIME_PAST) ? pindexPrev->GetMedianTimePast() : block.GetBlockTime();
    }

    // check coinstake version and reference hash requirements post fork
    if (IsMarch2022Enabled(consensusParams, pindexPrev))
    {
        // Deny PoW blocks after fork here unless on regtest, redundant with check in ConnectBlock
        if (block.IsProofOfWork())
        {
            if (Params().MineBlocksOnDemand() == false)
            {
                return state.DoS(
                    100, error("%s: pow blocks are not allowed", __func__), REJECT_INVALID, "bad-gen-type");
            }
        }
        else // coinstake
        {
            // enforce that the coinstake tx version is either 2 or 4
            if (block.vtx[1]->nVersion != 2 && block.vtx[1]->nVersion != 4)
            {
                return state.DoS(
                    100, error("%s: invalid version in coinstake tx", __func__), REJECT_INVALID, "bad-cs-version");
            }
            // we enforce that there must be a reference hash but it must be null until utxo checkpointing is
            // implemented and activated in a future fork
            if (!block.vtx[1]->referenceHash.IsNull())
            {
                return state.DoS(100, error("%s: non null reference hash in coinstake tx", __func__), REJECT_INVALID,
                    "bad-ref-hash");
            }
        }
    }

    // Check that all transactions are finalized and have proper versions / fields
    for (auto const &tx : block.vtx)
    {
        if (!IsFinalTx(*tx, nHeight, nLockTimeCutoff))
        {
            return state.DoS(
                10, error("%s: contains a non-final transaction", __func__), REJECT_INVALID, "bad-txns-nonfinal");
        }
        // restrict tx versions
        if (tx->nVersion > 4 || tx->nVersion <= 0)
        {
            return state.DoS(10, error("%s: contains a transaction with invalid version", __func__), REJECT_INVALID,
                "bad-txns-version-4");
        }
    }

    // Enforce rule that the coinbase starts with serialized block height
    CScript expect = CScript() << nHeight;
    if (block.vtx[0]->vin[0].scriptSig.size() < expect.size() ||
        !std::equal(expect.begin(), expect.end(), block.vtx[0]->vin[0].scriptSig.begin()))
    {
        return state.DoS(
            100, error("%s: block height mismatch in coinbase", __func__), REJECT_INVALID, "bad-cb-height");
    }

    return true;
}

bool ProcessNewBlock(CValidationState &state,
    const CChainParams &chainparams,
    const CNode *pfrom,
    const CBlock *pblock,
    bool fForceProcessing,
    CDiskBlockPos *dbp)
{
    // Preliminary checks
    bool checked = CheckBlock(*pblock, state); // no lock required

    {
        LOCK(cs_main);
        bool fRequested = g_requestman->MarkBlockAsReceived(pblock->GetHash());
        RECURSIVEWRITELOCK(g_chain.cs_mapBlockIndex);
        fRequested |= fForceProcessing;
        if (!checked)
        {
            LogPrintf("%s \n", state.GetRejectReason().c_str());
            return error("%s: CheckBlock FAILED", __func__);
        }

        // Store to disk
        CBlockIndex *pindex = nullptr;
        bool ret = AcceptBlock(pblock, state, chainparams, &pindex, fRequested, dbp);
        CheckBlockIndex(chainparams.GetConsensus());
        if (!ret)
        {
            return error("%s: AcceptBlock FAILED", __func__);
        }
    }

    if (!ActivateBestChain(state, chainparams, pblock))
    {
        if (state.IsInvalid() || state.IsError())
            return error("%s: ActivateBestChain failed", __func__);
        else
            return false;
    }
    return true;
}
