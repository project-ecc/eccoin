// Copyright (c) 2021 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ECCOIN_VALIDATION_FORKS_H
#define ECCOIN_VALIDATION_FORKS_H

#include "chain/chain.h"
#include "consensus/params.h"

/// MARCH 2022 ///
/** Test if March 2022 fork has activated */
bool IsMarch2022Enabled(const Consensus::Params &consensusparams, const CBlockIndex *pindexTip);
/** Check if the next will be the first block where the new set of rules will be enforced */
bool IsMarch2022Next(const Consensus::Params &consensusparams, const CBlockIndex *pindexTip);



#endif
