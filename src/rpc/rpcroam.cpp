// This file is part of the Eccoin project
// Copyright (c) 2021 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "beta.h"
#include "roam/aodv.h"
#include "roam/packetmanager.h"
#include "roam/routingtag.h"
#include "rpcserver.h"
#include "util/logger.h"
#include "util/utilstrencodings.h"
#include <univalue.h>

#include "main.h"

#include <sstream>

extern void EnsureTagDBIsUnlocked();

/*
void EnsureTagDBIsUnlocked()
{
    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }

    LOCK(g_connman->cs_tagstore);

    if (g_connman->tagstore->IsLocked())
    {
        throw JSONRPCError(
            RPC_WALLET_UNLOCK_NEEDED, "Error: Please enter the wallet passphrase with walletpassphrase first.");
    }
}
*/

UniValue roam_getroutingtable(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 0)
        throw std::runtime_error("roam.getroutingtable\n"
                                 "\nreturns the aodv routing table.\n"
                                 "\nResult:\n"
                                 "{\n"
                                 "   \"mapidkey\" : {\n"
                                 "       \"NodeId : Key,\"\n"
                                 "       ...\n"
                                 "   },\n"
                                 "   \"mapkeyid\" : {\n"
                                 "       \"Key : NodeId,\"\n"
                                 "       ...\n"
                                 "   }\n"
                                 "   \"direct peers\" : {\n"
                                 "       \"NodeId : Key,\"\n"
                                 "       ...\n"
                                 "   }\n"
                                 "}\n"
                                 "\nExamples:\n" +
                                 HelpExampleCli("getaodvtable", "") + HelpExampleRpc("getaodvtable", ""));
    std::map<NodeId, std::set<CPubKey> > IdKey;
    std::map<CPubKey, std::set<NodeId> > KeyId;
    std::map<NodeId, CPubKey> directConnect;
    g_aodvtable.GetRoutingTables(IdKey, KeyId, directConnect);
    UniValue obj(UniValue::VOBJ);
    UniValue IdKeyObj(UniValue::VOBJ);
    for (auto &entry : IdKey)
    {
        for (auto &path : entry.second)
        {
            IdKeyObj.push_back(Pair(std::to_string(entry.first), path.Raw64Encoded()));
        }
    }
    UniValue KeyIdObj(UniValue::VOBJ);
    for (auto &entry : KeyId)
    {
        for (auto &path : entry.second)
        {
            KeyIdObj.push_back(Pair(entry.first.Raw64Encoded(), path));
        }
    }
    UniValue DCObj(UniValue::VOBJ);
    for (auto &entry : directConnect)
    {
        DCObj.push_back(Pair(std::to_string(entry.first), entry.second.Raw64Encoded()));
    }
    obj.push_back(Pair("mapidkey", IdKeyObj));
    obj.push_back(Pair("mapkeyid", KeyIdObj));
    obj.push_back(Pair("direct peers", DCObj));
    return obj;
}


UniValue roam_getaodvkeyentry(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
        throw std::runtime_error(
            "roam.getaodvkeyentry \"keyhash\" \n"
            "\nChecks AODV routing table for a key with a hash that matches keyhash, returns its NodeId.\n"
            "\nArguments:\n"
            "1. \"keyhash\"   (string, required) The hash of the key of the desired entry\n"
            "\nNote: This call is fairly expensive due to number of hashes being done.\n"
            "\nExamples:\n" +
            HelpExampleCli("getaodvkeyentry", "\"keyhash\"") + HelpExampleRpc("getaodvkeyentry", "\"keyhash\""));

    std::string strKey = params[0].get_str();
    std::map<CPubKey, std::set<NodeId> > KeyId;
    g_aodvtable.GetKeyIdTable(KeyId);
    UniValue obj(UniValue::VOBJ);
    bool fInvalid = false;
    std::vector<uint8_t> vPubKey = DecodeBase64(strKey.c_str(), &fInvalid);
    if (fInvalid)
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed pubkey base64 encoding");
    }
    CPubKey pubkey(vPubKey.begin(), vPubKey.end());
    auto iter = KeyId.find(pubkey);
    if (iter == KeyId.end())
    {
        obj.push_back(Pair("Error", "no peer with this key found"));
    }
    else
    {
        for (auto &path : iter->second)
        {
            obj.push_back(Pair(iter->first.Raw64Encoded(), path));
        }
    }
    return obj;
}

UniValue roam_getaodvidentry(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
        throw std::runtime_error("roam.getaodvidentry \"nodeid\" \n"
                                 "\nChecks AODV routing table for the desired nodeid, returns its keys hash.\n"
                                 "\nArguments:\n"
                                 "1. \"nodeid\"   (number, required) The nodeid of the desired entry\n"
                                 "\nExamples:\n" +
                                 HelpExampleCli("getaodvidentry", "12") + HelpExampleRpc("getaodvidentry", "32"));

    int64_t peerId = params[0].get_int64();
    std::map<NodeId, std::set<CPubKey> > IdKey;
    g_aodvtable.GetIdKeyTable(IdKey);
    auto iter = IdKey.find(peerId);
    UniValue obj(UniValue::VOBJ);
    if (iter == IdKey.end())
    {
        obj.push_back(Pair("Error", "no peer with this id found"));
    }
    else
    {
        for (auto &path : iter->second)
        {
            obj.push_back(Pair(std::to_string(iter->first), path.Raw64Encoded()));
        }
    }
    return obj;
}

UniValue roam_getroutingpubkey(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() != 0)
    {
        throw std::runtime_error("roam.getroutingpubkey\n"
                                 "\nreturns the routing public key used by this node.\n"
                                 "\nResult:\n"
                                 "\"Key\" (string)\n"
                                 "\nExamples:\n" +
                                 HelpExampleCli("getroutingpubkey", "") + HelpExampleRpc("getroutingpubkey", ""));
    }
    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }
    return g_connman->GetPublicTagPubKey().Raw64Encoded();
}


UniValue roam_findroute(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
    {
        throw std::runtime_error(
            "roam.findroute\n"
            "\nattempts to find a route to the node with the given pub key.\n"
            "\nResult:\n"
            "\"None\n"
            "\nExamples:\n" +
            HelpExampleCli("findroute", "1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b") +
            HelpExampleRpc("findroute", "1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b"));
    }
    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }
    bool fInvalid = false;
    std::vector<unsigned char> vPubKey = DecodeBase64(params[0].get_str().c_str(), &fInvalid);
    if (fInvalid)
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed pubkey base64 encoding");
    }
    if (g_aodvtable.HaveRoute(CPubKey(vPubKey.begin(), vPubKey.end())))
    {
        return NullUniValue;
    }
    uint64_t nonce = 0;
    GetStrongRandBytes((uint8_t *)&nonce, sizeof(nonce));
    CPubKey key;
    RequestRouteToPeer(g_connman.get(), key, nonce, CPubKey(vPubKey.begin(), vPubKey.end()));
    return NullUniValue;
}

UniValue roam_haveroute(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() != 1)
    {
        throw std::runtime_error(
            "roam.haveroute\n"
            "\nchecks if a route to the node with the given pub key is known.\n"
            "\nResult:\n"
            "\"true/false\n"
            "\nExamples:\n" +
            HelpExampleCli("haveroute", "1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b") +
            HelpExampleRpc("haveroute", "1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b"));
    }
    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }
    bool fInvalid = false;
    std::vector<unsigned char> vPubKey = DecodeBase64(params[0].get_str().c_str(), &fInvalid);
    if (fInvalid)
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed pubkey base64 encoding");
    }
    return g_aodvtable.HaveRoute(CPubKey(vPubKey.begin(), vPubKey.end()));
}

UniValue roam_sendpacket(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || (params.size() != 3 && params.size() != 5))
    {
        throw std::runtime_error(
            "roam.sendpacket\n"
            "\nattempts to send a network packet to the destination\n"
            "\nArguments:\n"
            "1. \"key\"   (string, required) The key of the desired recipient\n"
            "2. \"protocolId\"   (number, required) The id of the protocol being used for the data\n"
            "3. \"Data\"   (vector of bytes, required) The desired data to be sent \n"
            "4. \"senderPubKey\"   (string, optional) Base 64 encoded pubkey used to make the signature \n"
            "5. \"signature\"   (string, optional) Base 64 encoded signature of the Data field using the senderPubKey "
            "\n"
            "Either both senderPubKey and signature are included or neither are. You cannot only include one of the two"
            "\nExamples:\n" +
            HelpExampleCli("sendpacket",
                "\"1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b\" 1 \"this is example data\"") +
            HelpExampleRpc("sendpacket",
                "\"1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b\", 1, \"this is example data\""));
    }
    bool fInvalid = false;
    std::vector<unsigned char> vPubKey = DecodeBase64(params[0].get_str().c_str(), &fInvalid);
    if (fInvalid)
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed base64 encoding");
    }
    uint16_t nProtocolId = (uint16_t)params[1].get_int();
    std::vector<uint8_t> vData = StrToBytes(params[2].get_str());
    bool result;
    if (params.size() == 5)
    {
        std::string strSenderPubKey = params[3].get_str();
        fInvalid = false;
        std::vector<uint8_t> vSignature = DecodeBase64(params[4].get_str().c_str(), &fInvalid);
        if (fInvalid)
        {
            throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed base64 encoding");
        }
        result = g_packetman.SendPacket(vPubKey, nProtocolId, vData, strSenderPubKey, vSignature);
    }
    else
    {
        result = g_packetman.SendPacket(vPubKey, nProtocolId, vData);
    }
    return result;
}

UniValue roam_registerbuffer(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() < 1 || params.size() > 2)
    {
        throw std::runtime_error(
            "roam.registerbuffer\n"
            "\nattempts to register the buffer for a network service protocol to a specific and returns the pubkey "
            "needed to read the buffer\n"
            "\nArguments:\n"
            "1. \"protocolId\"   (number, required) The id of the protocol being requested\n"
            "2. \"reject unsigned\"   (boolean, optional) The buffer will reject unsigned packets (default: false)\n"
            "\nExamples:\n" +
            HelpExampleCli("registerbuffer",
                "1 \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"") +
            HelpExampleRpc("registerbuffer",
                "1, \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\""));
    }
    uint16_t nProtocolId = (uint16_t)params[0].get_int();
    bool reject = false;
    if (params.size() == 2)
    {
        reject = params[1].get_bool();
    }
    std::string pubkey;
    if (g_packetman.RegisterBuffer(nProtocolId, pubkey, reject))
    {
        return pubkey;
    }
    throw JSONRPCError(RPC_INTERNAL_ERROR, "registerbuffer: failed to register buffer");
}

UniValue roam_getbuffer(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() != 2)
    {
        throw std::runtime_error(
            "roam.getbuffer\n"
            "\nattempts to get the buffer for a network service protocol\n"
            "\nArguments:\n"
            "1. \"protocolId\"   (number, required) The id of the protocol being requested\n"
            "2. \"signature\"   (string, required) The authentication signature required to request the buffer\n"
            "\nExamples:\n" +
            HelpExampleCli("getbuffer",
                "1 \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"") +
            HelpExampleRpc("getbuffer",
                "1, \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\""));
    }
    uint16_t nProtocolId = (uint16_t)params[0].get_int();
    std::string sig = params[1].get_str();
    std::vector<CPacket> bufferData;
    UniValue obj(UniValue::VOBJ);
    if (g_packetman.GetBuffer(nProtocolId, bufferData, sig))
    {
        uint64_t counter = 0;
        for (auto &entry : bufferData)
        {
            std::stringstream hexstream;
            hexstream << std::hex;
            for (uint8_t &byte : entry.GetData())
            {
                hexstream << std::setw(2) << std::setfill('0') << static_cast<int>(byte);
            }
            obj.push_back(Pair(std::to_string(counter), hexstream.str()));
            counter++;
        }
    }
    else
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "getbuffer failed, buffer not registered or invalid signature");
    }
    return obj;
}

UniValue roam_resetbuffertimeout(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() != 2)
    {
        throw std::runtime_error(
            "roam.resetbuffertimeout\n"
            "\na buffer keepalive, reset the buffer timeout timer.\n"
            "\nArguments:\n"
            "1. \"protocolId\"   (number, required) The id of the protocol being requested\n"
            "2. \"signature\"   (string, required) The authentication signature required to request the buffer\n"
            "\nExamples:\n" +
            HelpExampleCli("resetbuffertimeout",
                "1 \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"") +
            HelpExampleRpc("resetbuffertimeout",
                "1, \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\""));
    }
    uint16_t nProtocolId = (uint16_t)params[0].get_int();
    std::string sig = params[1].get_str();
    if (g_packetman.ResetBufferTimeout(nProtocolId, sig))
    {
        return true;
    }
    throw JSONRPCError(
        RPC_INVALID_ADDRESS_OR_KEY, "resetbuffertimeout failed, buffer not registered or invalid signature");
}

UniValue roam_releasebuffer(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() != 2)
    {
        throw std::runtime_error(
            "roam.releasebuffer\n"
            "\nattempts to release the buffer for a network service protocol\n"
            "\nArguments:\n"
            "1. \"protocolId\"   (number, required) The id of the protocol being requested\n"
            "2. \"signature\"   (string, required) The authentication signature required to release the buffer\n"
            "\nExamples:\n" +
            HelpExampleCli("releasebuffer",
                "1 \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"") +
            HelpExampleRpc("releasebuffer",
                "1, \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\""));
    }
    uint16_t nProtocolId = (uint8_t)params[0].get_int();
    std::string sig = params[1].get_str();
    return g_packetman.ReleaseBuffer(nProtocolId, sig);
}

UniValue roam_buffersignmessage(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() != 2)
    {
        throw std::runtime_error(
            "roam.buffersignmessage\n"
            "\nsigns a message with a provided base64 encoded pubkey\n"
            "\nArguments:\n"
            "1. \"pubkey\"   (string, required) The base64 encoded pubkey\n"
            "2. \"message\"  (string, required) The message to be signed\n"
            "\nResult:\n"
            "\"signature\"          (string) The signature of the message encoded in base 64\n"
            "\nExamples:\n" +
            HelpExampleCli("buffersignmessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/"
                                                "qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\" \"my message\"") +
            HelpExampleRpc("buffersignmessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/"
                                                "qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\", \"my message\""));
    }

    std::string strPubKey = params[0].get_str();
    std::string strMessage = params[1].get_str();

    CHashWriter ss(SER_GETHASH, 0);
    ss << strMessage;

    std::vector<unsigned char> vchSig;
    // restore the pubkey from the base64 encoded pubkey provided
    CPubKey pubkey(strPubKey);
    CKey key;
    if (!g_packetman.GetBufferKey(pubkey, key))
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "buffersignmessage: lookup failed");
    }
    if (!key.SignCompact(ss.GetHash(), vchSig))
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "buffersignmessage: Sign failed");
    }
    return EncodeBase64(&vchSig[0], vchSig.size());
}

std::string _tagsignmessage(const std::string &strMessage)
{
    CHashWriter ss(SER_GETHASH, 0);
    ss << strMessage;

    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }
    CRoutingTag pubtag;
    if (!g_connman->tagstore->GetTag(g_connman->tagstore->GetCurrentPublicTagPubKey().GetID(), pubtag))
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Sign failed, Tag error");
    }
    std::vector<unsigned char> vchSig;
    if (!pubtag.SignCompact(ss.GetHash(), vchSig))
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Sign failed");
    }
    return EncodeBase64(&vchSig[0], vchSig.size());
}

UniValue roam_tagsignmessage(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() != 1)
    {
        throw std::runtime_error("roam.tagsignmessage\n"
                                 "\nsigns a message with your public tag\n"
                                 "\nArguments:\n"
                                 "1. \"message\"   (string, required) The message to be signed\n"
                                 "\nResult:\n"
                                 "\"signature\"          (string) The signature of the message encoded in base 64\n"
                                 "\nExamples:\n" +
                                 HelpExampleCli("tagsignmessage", "hello. sign this message") +
                                 HelpExampleRpc("tagsignmessage", "hello. sign this message"));
    }
    std::string strMessage = params[0].get_str();
    return _tagsignmessage(strMessage);
}

bool _tagverifymessage(const std::string &strPubKey_base64, const std::string &strSign, const std::string &strMessage)
{
    CHashWriter ss(SER_GETHASH, 0);
    ss << strMessage;

    bool fInvalid = false;
    std::vector<unsigned char> vchSig = DecodeBase64(strSign.c_str(), &fInvalid);

    if (fInvalid)
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed base64 encoding");
    }
    CPubKey pubcompare(strPubKey_base64);
    CPubKey pubkey;
    if (!pubkey.RecoverCompact(ss.GetHash(), vchSig))
    {
        return false;
    }

    return (pubkey.GetID() == pubcompare.GetID());
}

UniValue roam_tagverifymessage(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }
    if (fHelp || params.size() != 3)
    {
        throw std::runtime_error(
            "roam.tagverifymessage \"ecc address\" \"signature\" \"message\"\n"
            "\nVerify a signed message\n"
            "\nArguments:\n"
            "1. \"pubkey\"  (string, required) The base64 encoded tag pubkey to use for the signature\n"
            "2. \"signature\"       (string, required) The signature provided by the signer in base 64 encoding (see "
            "signmessage).\n"
            "3. \"message\"         (string, required) The message that was signed.\n"
            "\nResult:\n"
            "true|false   (boolean) If the signature is verified or not.\n"
            "\nExamples:\n"
            "\nUnlock the wallet for 30 seconds\n" +
            HelpExampleCli("walletpassphrase", "\"mypassphrase\" 30") + "\nCreate the signature\n" +
            HelpExampleCli("signmessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/"
                                          "qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\" \"my message\"") +
            "\nVerify the signature\n" +
            HelpExampleCli("tagverifymessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/"
                                               "qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\" \"signature\" \"my message\"") +
            "\nAs json rpc\n" +
            HelpExampleRpc("tagverifymessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/"
                                               "qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\", \"signature\", \"my message\""));
    }
    std::string strPubKey_base64 = params[0].get_str();
    std::string strSign = params[1].get_str();
    std::string strMessage = params[2].get_str();
    return _tagverifymessage(strPubKey_base64, strSign, strMessage);
}

UniValue roam_dumptags(const UniValue &params, bool fHelp)
{
    if (fHelp || params.size() != 1)
    {
        throw std::runtime_error("roam.dumptags \"filename\"\n"
                                 "\nDumps all the roam tags in a human-readable format.\n"
                                 "\nArguments:\n"
                                 "1. \"filename\"    (string, required) The filename\n"
                                 "\nExamples:\n" +
                                 HelpExampleCli("dumproamtags", "\"test\"") +
                                 HelpExampleRpc("dumproamtags", "\"test\""));
    }
    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }

    EnsureTagDBIsUnlocked();

    std::ofstream file;
    file.open(params[0].get_str().c_str());
    if (!file.is_open())
    {
        throw JSONRPCError(RPC_INVALID_PARAMETER, "Cannot open wallet dump file");
    }
    {
        LOCK(g_connman->cs_tagstore);
        CNetTagStore *tagstore = g_connman->tagstore;

        // produce output
        file << strprintf("# Roam Tag DB dump created by Eccoin %s (%s)\n", CLIENT_BUILD, CLIENT_DATE);
        file << strprintf("# * Created on %s\n", EncodeDumpTime(GetTime()));
        file << "\n";

        for (const auto &entry : tagstore->mapTags)
        {
            const CKeyID &keyid = entry.first;
            CRoutingTag tagOut;
            tagstore->GetTag(keyid, tagOut);
            file << strprintf("Tag Private Key=%s, IsPrivate=%s, Tag Public Key=%s\n",
                CTagSecret(tagOut.GetPrivKey()).ToString().c_str(), tagOut.IsPrivate() ? "true" : "false",
                tagOut.GetPubKey().Raw64Encoded().c_str());
        }
        file << "\n";
        file << "# End of dump\n";
        file.close();
    }
    return NullUniValue;
}

UniValue roam_importtagkey(const UniValue &params, bool fHelp)
{
    if (fHelp || params.size() != 1)
        throw std::runtime_error("roam.importprivkey \"tagprivkey\"\n"
                                 "\nAdds a private key (as returned by dumptagkey) to your tagdb.\n"
                                 "\nArguments:\n"
                                 "1. \"tagprivkey\"   (string, required) The private key (see dumptagkey)\n"
                                 "\nExamples:\n"
                                 "\nDump a private key\n" +
                                 HelpExampleCli("dumptagkey", "\"mytag\"") + "\nImport\n" +
                                 HelpExampleCli("importtagkey", "\"mykey\"") + "\nAs a JSON-RPC call\n" +
                                 HelpExampleRpc("importtagkey", "\"mykey\""));


    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }

    EnsureTagDBIsUnlocked();

    std::string strSecret = params[0].get_str();

    CTagSecret vchSecret;
    bool fGood = vchSecret.SetString(strSecret);

    if (!fGood)
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Invalid private key encoding");
    }

    CKey key = vchSecret.GetKey();
    if (!key.IsValid())
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Private key outside allowed range");
    }

    CPubKey pubkey = key.GetPubKey();
    assert(key.VerifyPubKey(pubkey));
    CKeyID vchAddress = pubkey.GetID();
    {
        LOCK(g_connman->cs_tagstore);
        CNetTagStore *tagstore = g_connman->tagstore;
        // Don't throw error in case a key is already there
        if (tagstore->HaveTag(vchAddress))
        {
            return NullUniValue;
        }
        // TODO: for now, assume it is a public tag, in the future add a second param to use for this value
        // for safety that param should default to false
        CRoutingTag newtag(false, pubkey, key.GetPrivKey());
        if (!tagstore->AddTag(newtag))
        {
            throw JSONRPCError(RPC_WALLET_ERROR, "Error adding tag to routingdb");
        }
    }

    return NullUniValue;
}

UniValue roam_dumptagkey(const UniValue &params, bool fHelp)
{
    if (fHelp || params.size() != 1)
    {
        throw std::runtime_error("roam.dumpttagkey \"tagpubkey\"\n"
                                 "\nReveals the private key corresponding to 'tagpubkey'.\n"
                                 "Then the importtagkey can be used with this output\n"
                                 "\nArguments:\n"
                                 "1. \"tagpubkey\"   (string, required) The tag public key for the private key\n"
                                 "\nResult:\n"
                                 "\"key\"                (string) The private key\n"
                                 "\nExamples:\n" +
                                 HelpExampleCli("dumptagkey", "\"tagpubkey\"") +
                                 HelpExampleCli("importtagkey", "\"mykey\"") +
                                 HelpExampleRpc("dumptagkey", "\"tagpubkey\""));
    }

    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }

    EnsureTagDBIsUnlocked();

    std::string strTagPubKey = params[0].get_str();
    CPubKey tagPubKey(strTagPubKey);
    CKeyID keyID = tagPubKey.GetID();
    CRoutingTag tagOut;
    {
        LOCK(g_connman->cs_tagstore);
        if (!g_connman->tagstore->GetTag(keyID, tagOut))
        {
            throw JSONRPCError(RPC_WALLET_ERROR, "Private key for address " + strTagPubKey + " is not known");
        }
    }
    return CTagSecret(tagOut.GetPrivKey()).ToString();
}
