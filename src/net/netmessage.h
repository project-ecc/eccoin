// This file is part of the Eccoin project
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2020 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef ECCOIN_NET_NETMESSAGE_H
#define ECCOIN_NET_NETMESSAGE_H

#include "crypto/hash.h"
#include "protocol.h"
#include "streams.h"

class CNetMessage
{
private:
    mutable CHash256 hasher;

public:
    // Parsing header (false) or data (true)
    bool in_data;

    // Partially received header.
    CDataStream hdrbuf;
    // Complete header.
    CMessageHeader hdr;
    unsigned int nHdrPos;

    // Received message data.
    CDataStream vRecv;
    unsigned int nDataPos;

    // Time (in microseconds) of message receipt.
    int64_t nTime;

    // default constructor builds an empty message object to accept assignment of real messages
    CNetMessage() : hdrbuf(0, 0), hdr({0, 0, 0, 0}), vRecv(0, 0)
    {
        hdrbuf.resize(24);
        in_data = false;
        nHdrPos = 0;
        nDataPos = 0;
        nTime = 0;
    }

    CNetMessage(const CMessageHeader::MessageMagic &pchMessageStartIn, int nTypeIn, int nVersionIn)
        : hdrbuf(nTypeIn, nVersionIn), hdr(pchMessageStartIn), vRecv(nTypeIn, nVersionIn)
    {
        hdrbuf.resize(24);
        in_data = false;
        nHdrPos = 0;
        nDataPos = 0;
        nTime = 0;
    }

    bool complete() const
    {
        if (!in_data)
        {
            return false;
        }

        return (hdr.nMessageSize == nDataPos);
    }

    void SetVersion(int nVersionIn)
    {
        hdrbuf.SetVersion(nVersionIn);
        vRecv.SetVersion(nVersionIn);
    }

    int readHeader(const char *pch, unsigned int nBytes)
    {
        // copy data to temporary parsing buffer
        unsigned int nRemaining = 24 - nHdrPos;
        unsigned int nCopy = std::min(nRemaining, nBytes);
        memcpy(&hdrbuf[nHdrPos], pch, nCopy);
        nHdrPos += nCopy;
        // if header incomplete, exit
        if (nHdrPos < 24)
        {
            return nCopy;
        }
        // deserialize to CMessageHeader
        try
        {
            hdrbuf >> hdr;
        }
        catch (const std::exception &)
        {
            return -1;
        }
        // reject messages larger than MAX_SIZE
        if (hdr.nMessageSize > MAX_SIZE)
        {
            return -1;
        }
        // switch state to reading message data
        in_data = true;
        return nCopy;
    }

    int readData(const char *pch, unsigned int nBytes)
    {
        unsigned int nRemaining = hdr.nMessageSize - nDataPos;
        unsigned int nCopy = std::min(nRemaining, nBytes);
        if (vRecv.size() < nDataPos + nCopy)
        {
            // Allocate up to 256 KiB ahead, but never more than the total message
            // size.
            vRecv.resize(std::min(hdr.nMessageSize, nDataPos + nCopy + 256 * 1024));
        }
        hasher.Write((const uint8_t *)pch, nCopy);
        memcpy(&vRecv[nDataPos], pch, nCopy);
        nDataPos += nCopy;
        return nCopy;
    }
};

#endif
