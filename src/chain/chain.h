// This file is part of the Eccoin project
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2021 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_CHAIN_H
#define BITCOIN_CHAIN_H

#include "arith_uint256.h"
#include "block.h"
#include "blockindex.h"
#include "chain/chainparams.h"
#include "pow.h"
#include "tinyformat.h"
#include "uint256.h"

#include <atomic>
#include <unordered_map>
#include <vector>

struct BlockHasher
{
    size_t operator()(const uint256 &hash) const
    {
        return hash.GetCheapHash();
    }
};
typedef std::unordered_map<uint256, CBlockIndex *, BlockHasher> BlockMap;

/** An in-memory indexed chain of blocks. */
class CChain
{
private:
    std::vector<CBlockIndex *> vChain;
    std::atomic<CBlockIndex *> tip;

    bool _Contains(const CBlockIndex *pindex) const
    {
        if (pindex == nullptr || pindex->nHeight < 0 || pindex->nHeight >= (int)vChain.size())
        {
            return false;
        }
        // We can return this outside of the lock because CBlockIndex objects are never deleted
        return (vChain[pindex->nHeight] == pindex);
    }

public:
    mutable CRecursiveSharedCriticalSection cs_mapBlockIndex;

    /** map containing all block indexs ever seen for this chain */
    BlockMap mapBlockIndex GUARDED_BY(cs_mapBlockIndex);

    /** Best header we've seen so far (used for getheaders queries' starting points). */
    std::atomic<CBlockIndex *> pindexBestHeader;

private:
    bool LoadBlockIndexDB();

public:
    CChain()
    {
        mapBlockIndex.clear();
        pindexBestHeader = nullptr;
        tip = nullptr;
    }
    ~CChain()
    {
        // block headers
        pindexBestHeader = nullptr;
        tip = nullptr;
        vChain.clear();
        BlockMap::iterator it1 = mapBlockIndex.begin();
        for (; it1 != mapBlockIndex.end(); it1++)
        {
            delete (*it1).second;
        }
        mapBlockIndex.clear();
    }
    /** Returns the index entry for the genesis block of this chain, or NULL if none. */
    CBlockIndex *Genesis() const
    {
        return (*this)[0];
    }
    /** Returns the index entry for the tip of this chain, or NULL if none. */
    CBlockIndex *Tip() const
    {
        return tip;
    }
    /** Returns the index entry at a particular height in this chain, or NULL if no such height exists. */
    CBlockIndex *operator[](int nHeight) const
    {
        RECURSIVEREADLOCK(cs_mapBlockIndex);
        if (nHeight < 0 || nHeight >= (int)vChain.size())
        {
            return nullptr;
        }
        return vChain[nHeight];
    }

    /** Compare two chains efficiently. */
    friend bool operator==(const CChain &a, const CChain &b)
    {
        RECURSIVEREADLOCK(a.cs_mapBlockIndex);
        RECURSIVEREADLOCK(b.cs_mapBlockIndex);
        return a.vChain.size() == b.vChain.size() && a.vChain[a.vChain.size() - 1] == b.vChain[b.vChain.size() - 1];
    }

    void operator=(const CChain &a)
    {
        vChain = a.vChain;
        tip.store(a.tip.load());
    }

    /** Efficiently check whether a block is present in this chain. */
    bool Contains(const CBlockIndex *pindex) const
    {
        return (*this)[pindex->nHeight] == pindex;
    }
    /** Find the successor of a block in this chain, or NULL if the given index is not found or is the tip. */
    CBlockIndex *Next(const CBlockIndex *pindex) const
    {
        RECURSIVEREADLOCK(cs_mapBlockIndex);
        if (pindex)
        {
            int nextHeight = pindex->nHeight + 1;
            if (_Contains(pindex) && nextHeight >= 0 && nextHeight < (int)vChain.size())
            {
                return vChain[nextHeight];
            }
        }
        return nullptr;
    }

    /** Return the maximal height in the chain. Is equal to chain.Tip() ? chain.Tip()->nHeight : -1. */
    int32_t Height() const
    {
        return tip.load() ? tip.load()->nHeight : -1;
    }
    /** Set/initialize a chain with a given tip. */
    void SetTip(CBlockIndex *pindex);

    /** Return a CBlockLocator that refers to a block in this chain (by default the tip). */
    CBlockLocator GetLocator(const CBlockIndex *pindex = nullptr) const;

    /** Find the last common block between this chain and a block index entry. */
    const CBlockIndex *FindFork(const CBlockIndex *pindex) const;

    /** Look up the block index entry for a given block hash. returns nullptr if it does not exist */
    CBlockIndex *LookupBlockIndex(const uint256 &hash);

    /** Add a new block index entry for a given block recieved from the network */
    CBlockIndex *AddToBlockIndex(const CBlockHeader &block);

    /** Find the last common block between the parameter chain and a locator. */
    CBlockIndex *FindForkInGlobalIndex(const CChain &chain, const CBlockLocator &locator);

    /** Check whether we are doing an initial block download (synchronizing from disk or network) */
    bool IsInitialBlockDownload();

    /** Initialize a new block tree database + block data on disk */
    bool InitBlockIndex(const CChainParams &chainparams);

    /** Create a new block index entry for a given block hash loaded from disk*/
    CBlockIndex *InsertBlockIndex(uint256 hash);

    /** Load the block tree and coins database from disk */
    bool LoadBlockIndex();

    /** Import blocks from an external file */
    bool LoadExternalBlockFile(const CChainParams &chainparams, FILE *fileIn, CDiskBlockPos *dbp = NULL);

    /** Unload database information */
    void UnloadBlockIndex();
};

extern CChain g_chain;

#endif // BITCOIN_CHAIN_H
