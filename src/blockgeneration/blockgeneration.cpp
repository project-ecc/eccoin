// This file is part of the Eccoin project
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "blockgeneration.h"
#include "compare.h"
#include "consensus/merkle.h"
#include "miner.h"
#include "minter.h"
#include "util/util.h"
#include "wallet/wallet.h"

std::atomic<bool> shutdown_miner_threads(false);
std::atomic<bool> shutdown_minter_threads(false);

void IncrementExtraNonce(CBlock *pblock, CBlockIndex *pindexPrev, unsigned int &nExtraNonce)
{
    // Update nExtraNonce
    static uint256 hashPrevBlock;
    if (hashPrevBlock != pblock->hashPrevBlock)
    {
        nExtraNonce = 0;
        hashPrevBlock = pblock->hashPrevBlock;
    }
    ++nExtraNonce;
    unsigned int nHeight = pindexPrev->nHeight + 1; // Height first in coinbase required for block.version=2
    pblock->vtx[0]->vin[0].scriptSig = (CScript() << nHeight << CScriptNum(nExtraNonce)) + COINBASE_FLAGS;
    assert(pblock->vtx[0]->vin[0].scriptSig.size() <= 100);

    pblock->hashMerkleRoot = BlockMerkleRoot(*pblock);
}

int64_t UpdateTime(CBlockHeader *pblock, const Consensus::Params &consensusParams, const CBlockIndex *pindexPrev)
{
    int64_t nOldTime = pblock->nTime;
    int64_t nNewTime = std::max(pindexPrev->GetMedianTimePast() + 1, GetAdjustedTime());

    if (nOldTime < nNewTime)
        pblock->nTime = nNewTime;

    // Updating time can change work required on testnet:
    if (consensusParams.fPowAllowMinDifficultyBlocks)
        pblock->nBits = GetNextTargetRequired(pindexPrev, false);

    return nNewTime - nOldTime;
}

std::unique_ptr<CBlockTemplate> CreateNewBlock(CWallet *pwallet, const CScript &scriptPubKeyIn, bool fProofOfStake)
{
    if (fProofOfStake)
    {
        return CreateNewPoSBlock(pwallet, scriptPubKeyIn);
    }
    return CreateNewPoWBlock(pwallet, scriptPubKeyIn);
}

CCriticalSection cs_minerThreads;
thread_group *minerThreads = nullptr;

void ThreadMiner(void *parg, int32_t nThreads, bool shutdownOnly)
{
    LOCK(cs_minerThreads);
    // if the thread group does not exist, create it
    if (minerThreads == nullptr)
    {
        minerThreads = new thread_group(&shutdown_miner_threads);
    }
    // if mining was off and threads is 0, auto detect number of threads
    if (nThreads == 0 && minerThreads->empty())
    {
        nThreads = GetNumCores() / 2;
    }
    // if we are shutting down, clean up all of the threads
    // and delete the thread group
    if (shutdownOnly)
    {
        minerThreads->interrupt_all();
        minerThreads->join_all();
        delete minerThreads;
        minerThreads = nullptr;
        return;
    }
    CWallet *pwallet = (CWallet *)parg;
    // if we are not shutting down, toggle the threads
    try
    {
        if (minerThreads->empty())
        {
            shutdown_miner_threads.store(false);
            for (int32_t i = 0; i < nThreads; ++i)
            {
                minerThreads->create_thread(&EccMiner, pwallet);
            }
        }
        // mining was on but nThreads is 0, shutdown all of the threads
        else if (nThreads == 0)
        {
            minerThreads->interrupt_all();
            minerThreads->join_all(true);
        }
        // set the number of threads to the new desired number
        else
        {
            int32_t nThreadsRemaining = nThreads - (int32_t)minerThreads->size();
            // if we are to add more threads...
            if (nThreadsRemaining >= 0)
            {
                for (int32_t i = 0; i < nThreadsRemaining; ++i)
                {
                    minerThreads->create_thread(&EccMiner, pwallet);
                }
            }
            else // we want to remove some threads
            {
                // interrupt all threads as the killswitch is bound to all of them
                // and there is no good way to only turn off some
                minerThreads->interrupt_all();
                minerThreads->join_all(true);
                // wait 1 second to give threads time to quit
                MilliSleep(1000);
                // spawn the desired number of threads
                shutdown_miner_threads.store(false);
                for (int32_t i = 0; i < nThreads; ++i)
                {
                    minerThreads->create_thread(&EccMiner, pwallet);
                }
            }
        }
    }
    catch (std::exception &e)
    {
        PrintException(&e, "ThreadECCMiner()");
    }
    catch (...)
    {
        PrintException(NULL, "ThreadECCMiner()");
    }
    nHPSTimerStart = 0;
    dHashesPerSec = 0;
}

CCriticalSection cs_minterThreads;
thread_group *minterThreads = nullptr;

void ThreadMinter(void *parg, int32_t nThreads, bool shutdownOnly)
{
    LOCK(cs_minterThreads);
    // if the thread group does not exist, create it
    if (minterThreads == nullptr)
    {
        minterThreads = new thread_group(&shutdown_minter_threads);
    }
    // if mining was off and threads is 0, auto detect number of threads
    if (nThreads == 0 && minterThreads->empty())
    {
        nThreads = GetNumCores() / 2;
    }
    // temporarily restrict the number of threads to 1 until createcoinstake
    // can be reworked to divide work amongst different threads properly
    if (nThreads > 1)
    {
        nThreads = 1;
    }
    // if we are shutting down, clean up all of the threads
    // and delete the thread group
    if (shutdownOnly)
    {
        minterThreads->interrupt_all();
        minterThreads->join_all();
        delete minterThreads;
        minterThreads = nullptr;
        return;
    }
    CWallet *pwallet = (CWallet *)parg;
    // if we are not shutting down, toggle the threads
    try
    {
        if (minterThreads->empty())
        {
            shutdown_minter_threads.store(false);
            for (int32_t i = 0; i < nThreads; ++i)
            {
                minterThreads->create_thread(&EccMinter, pwallet);
            }
        }
        // mining was on but nThreads is 0, shutdown all of the threads
        else if (nThreads == 0)
        {
            minterThreads->interrupt_all();
            minterThreads->join_all(true);
        }
        // set the number of threads to the new desired number
        else
        {
            int32_t nThreadsRemaining = nThreads - (int32_t)minterThreads->size();
            // if we are to add more threads...
            if (nThreadsRemaining >= 0)
            {
                for (int32_t i = 0; i < nThreadsRemaining; ++i)
                {
                    minterThreads->create_thread(&EccMinter, pwallet);
                }
            }
            else // we want to remove some threads
            {
                // interrupt all threads as the killswitch is bound to all of them
                // and there is no good way to only turn off some
                minterThreads->interrupt_all();
                minterThreads->join_all(true);
                // wait 1 second to give threads time to quit
                MilliSleep(1000);
                // spawn the desired number of threads
                shutdown_minter_threads.store(false);
                for (int32_t i = 0; i < nThreads; ++i)
                {
                    minterThreads->create_thread(&EccMinter, pwallet);
                }
            }
        }
    }
    catch (std::exception &e)
    {
        PrintException(&e, "ThreadECCMinter()");
    }
    catch (...)
    {
        PrintException(NULL, "ThreadECCMinter()");
    }
}

void ThreadGeneration(void *parg, int32_t nThreads, bool shutdownOnly, bool fProofOfStake)
{
    if (fProofOfStake)
    {
        ThreadMinter(parg, nThreads, shutdownOnly);
    }
    else
    {
        ThreadMiner(parg, nThreads, shutdownOnly);
    }
}
