# Mac macOS Build Instructions and Notes
The commands in this guide should be executed in a Terminal application.
The built-in one is located in `/Applications/Utilities/Terminal.app`.

## Preparation
Install the macOS command line tools:

```bash
xcode-select --install
```

When the popup appears, click `Install`.

Then install [Homebrew](http://brew.sh).

## Dependencies

```bash
brew install automake berkeley-db4 libtool boost --c++11 miniupnpc openssl pkg-config protobuf --c++11 qt5 libevent
```

See [dependencies.md](dependencies.md) for a complete overview.

If you want to build the disk image with `make deploy` (.dmg / optional), you need RSVG

```bash
brew install librsvg
```

NOTE: Must build with QT 5.3 or higher. Building with Qt4 is not supported.

## Build Eccoind

1. Clone the Eccoin source code and cd into `eccoin`
    ```bash
    git clone https://gitlab.com/project-ecc/eccoin.git
    cd eccoin/
    ```

2.  Build:

    Configure and build the headless eccoin binary.

    ```bash
    ./autogen.sh
    ./configure
    make
    ```

3.  It is recommended to build and run the unit tests:

    ```bash
    make check
    ```

4.  You can also create a .dmg that contains the .app bundle (optional):

    ```bash
    make deploy
    ```

## Running

Eccoind is now available at `./src/eccoind`

Before running, it's recommended you create an RPC configuration file.

```bash
echo -e "rpcuser=rpcuser\nrpcpassword=$(xxd -l 16 -p /dev/urandom)" > "/Users/${USER}/Library/Application Support/eccoin/eccoin.conf"

chmod 600 "/Users/${USER}/Library/Application Support/eccoin/eccoin.conf"
```

The first time you run eccoind, it will start downloading the blockchain. This process could take several hours.

You can monitor the download process by looking at the debug.log file:

```bash
tail -f $HOME/Library/Application\ Support/eccoin/debug.log
```

## Other commands:

```bash
./src/eccoind -daemon # Starts the eccoind daemon.
./src/eccoind --help # Outputs a list of command-line options.
./src/eccoind help # Outputs a list of RPC commands when the daemon is running.
```
