#!/usr/bin/env python3
# Copyright (c) 2015 The Bitcoin Core developers
# Copyright (c) 2015-2017 The Bitcoin Unlimited developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.
import test_framework.loginit
from test_framework.test_framework import BitcoinTestFramework
from test_framework.util import *


class SignRawTransactionsTest(BitcoinTestFramework):
    """Tests transaction signing via RPC command "signrawtransaction"."""

    def setup_chain(self):
        print('Initializing test directory ' + self.options.tmpdir)
        initialize_chain_clean(self.options.tmpdir, 1)

    def setup_network(self, split=False):
        self.nodes = start_nodes(1, self.options.tmpdir, [["-marchforktime=1536781520"]])
        self.is_network_split = False

    def successful_signing_test(self):
        """Creates and signs a valid raw transaction with one input.

        Expected results:

        1) The transaction has a complete set of signatures
        2) No script verification error occurred"""
        privKeys = ['QdodTgE3WNLaintBgjFBcT9jZ9oAsJbcWTUXS1P7N39vd6WngVbE'] # MkATgTWCtA6tjGFqSPGszDErGZ2xhfvpHJ

        inputs = [
            # Valid pay-to-pubkey script
            {'txid': 'b81f38958edb228a49c507a7eaf6d557fcc9aef226539b0b484c9ff20d53fbb2', 'vout': 0,
             'scriptPubKey': '76a9148dc95547d0a49a05653aa7b4b816d32e02c2447b88ac', 'amount': 45}
        ]

        outputs = {'MazcSZErmng5CrxrvZWtmjnodb2eCx3gCv': 42}

        rawTx = self.nodes[0].createrawtransaction(inputs, outputs)
        rawTxSigned = self.nodes[0].signrawtransaction(rawTx, inputs, privKeys)

        # 1) The transaction has a complete set of signatures
        assert 'complete' in rawTxSigned
        assert_equal(rawTxSigned['complete'], True)

        # 2) No script verification error occurred
        assert 'errors' not in rawTxSigned

        #### Make sure you can sign with schnorr signatures
        rawTxSigned_schnorr = self.nodes[0].signrawtransaction(rawTx, inputs, privKeys, "ALL", "1")
        rawTxSigned_schnorr2 = self.nodes[0].signrawtransaction(rawTx, inputs, privKeys, "ALL", "schnorr")
        rawTxSigned_schnorr3 = self.nodes[0].signrawtransaction(rawTx, inputs, privKeys, "ALL", "SCHNORR")
        assert_equal(rawTxSigned_schnorr, rawTxSigned_schnorr2)
        assert_equal(rawTxSigned_schnorr, rawTxSigned_schnorr3)
        # check that different sig types were used
        assert_not_equal(rawTxSigned, rawTxSigned_schnorr)

        # 1) The transaction has a complete set of signatures
        assert 'complete' in rawTxSigned_schnorr
        assert_equal(rawTxSigned_schnorr['complete'], True)

        # 2) No script verification error occurred
        assert 'errors' not in rawTxSigned_schnorr

    def script_verification_error_test(self):
        """Creates and signs a raw transaction with valid (vin 0), invalid (vin 1) and one missing (vin 2) input script.

        Expected results:

        3) The transaction has no complete set of signatures
        4) Two script verification errors occurred
        5) Script verification errors have certain properties ("txid", "vout", "scriptSig", "sequence", "error")
        6) The verification errors refer to the invalid (vin 1) and missing input (vin 2)"""
        privKeys = ['QeLGUeRXTeWssskfXjh2vg5vVasxCYi2Y1QZbnXNGLMaUgtV8rQb']

        inputs = [
            # Valid pay-to-pubkey script
            {'txid': 'e3c635402be27cd96a332b8dbb11cb0b81b99f42c15b99522d5048c83981e80d', 'vout': 0},
            # Invalid script
            {'txid': '5b8673686910442c644b1f4993d8f7753c7c8fcb5c87ee40d56eaeef25204547', 'vout': 7},
            # Missing scriptPubKey
            {'txid': 'e3c635402be27cd96a332b8dbb11cb0b81b99f42c15b99522d5048c83981e80d', 'vout': 1},
        ]

        scripts = [
            # Valid pay-to-pubkey script
            {'txid': 'e3c635402be27cd96a332b8dbb11cb0b81b99f42c15b99522d5048c83981e80d', 'vout': 0, 'amount':25,
             'scriptPubKey': '76a91441c61c458b3a47bd108714bef5292f079123cf0c88ac'},
            # Invalid script
            {'txid': '5b8673686910442c644b1f4993d8f7753c7c8fcb5c87ee40d56eaeef25204547', 'vout': 7, 'amount':1.618,
             'scriptPubKey': 'badbadbadbad'}
        ]

        outputs = {'Mtk2ZJw5jL1C39Nk3mdkqwDeEgw5FwS5hU': 0.1}

        rawTx = self.nodes[0].createrawtransaction(inputs, outputs)
        rawTxSigned = self.nodes[0].signrawtransaction(rawTx, scripts, privKeys)

        # 3) The transaction has no complete set of signatures
        assert 'complete' in rawTxSigned
        assert_equal(rawTxSigned['complete'], False)

        # 4) Two script verification errors occurred
        assert 'errors' in rawTxSigned
        assert_equal(len(rawTxSigned['errors']), 2)

        # 5) Script verification errors have certain properties
        assert 'txid' in rawTxSigned['errors'][0]
        assert 'vout' in rawTxSigned['errors'][0]
        assert 'scriptSig' in rawTxSigned['errors'][0]
        assert 'sequence' in rawTxSigned['errors'][0]
        assert 'error' in rawTxSigned['errors'][0]

        # 6) The verification errors refer to the invalid (vin 1) and missing input (vin 2)
        assert_equal(rawTxSigned['errors'][0]['txid'], inputs[1]['txid'])
        assert_equal(rawTxSigned['errors'][0]['vout'], inputs[1]['vout'])
        assert_equal(rawTxSigned['errors'][1]['txid'], inputs[2]['txid'])
        assert_equal(rawTxSigned['errors'][1]['vout'], inputs[2]['vout'])

                # now run the same test with schnorr
        rawTxSigned2 = self.nodes[0].signrawtransaction(rawTx, scripts, privKeys, "ALL", "1")
        # check that different sig types were used
        assert_not_equal(rawTxSigned, rawTxSigned2)

        # 3) The transaction has no complete set of signatures
        assert 'complete' in rawTxSigned2
        assert_equal(rawTxSigned2['complete'], False)

        # 4) Two script verification errors occurred
        assert 'errors' in rawTxSigned2
        assert_equal(len(rawTxSigned2['errors']), 2)

        # 5) Script verification errors have certain properties
        assert 'txid' in rawTxSigned2['errors'][0]
        assert 'vout' in rawTxSigned2['errors'][0]
        assert 'scriptSig' in rawTxSigned2['errors'][0]
        assert 'sequence' in rawTxSigned2['errors'][0]
        assert 'error' in rawTxSigned2['errors'][0]

        # 6) The verification errors refer to the invalid (vin 1) and missing input (vin 2)
        assert_equal(rawTxSigned2['errors'][0]['txid'], inputs[1]['txid'])
        assert_equal(rawTxSigned2['errors'][0]['vout'], inputs[1]['vout'])
        assert_equal(rawTxSigned2['errors'][1]['txid'], inputs[2]['txid'])
        assert_equal(rawTxSigned2['errors'][1]['vout'], inputs[2]['vout'])


    def run_test(self):
        # mine some blocks to make sure we are past fork time
        self.nodes[0].generate(35)
        self.sync_blocks()
        self.successful_signing_test()
        self.script_verification_error_test()


if __name__ == '__main__':
    SignRawTransactionsTest().main()

def Test():
    t = SignRawTransactionsTest()
    t.drop_to_pdb = True
    bitcoinConf = {
        "debug": ["net", "blk", "thin", "mempool", "req", "bench", "evict"],  # "lck"
        "blockprioritysize": 2000000  # we don't want any transactions rejected due to insufficient fees...
    }
    t.main(["--tmppfx=/ramdisk/test"], bitcoinConf, None)  # , "--tracerpc"])
