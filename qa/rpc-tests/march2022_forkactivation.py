#!/usr/bin/env python3
# Copyright (c) 2018 The Bitcoin Unlimited developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.
import test_framework.loginit
import time
import sys
if sys.version_info[0] < 3:
    raise "Use Python 3"
import logging

from test_framework.test_framework import BitcoinTestFramework
from test_framework.util import *

maxSubsidy = 100 * COIN

class ForkTest (BitcoinTestFramework):

    def setup_chain(self, bitcoinConfDict=None, wallets=None):
        print("Initializing test directory " + self.options.tmpdir)
        initialize_chain_clean(self.options.tmpdir, 2, bitcoinConfDict, wallets)

    def setup_network(self, split=False):
        self.nodes = []
        self.nodes.append(start_node(0, self.options.tmpdir, ["-debug=wallet,kernel"]))
        self.nodes.append(start_node(1, self.options.tmpdir, ))
        connect_nodes_bi(self.nodes, 0, 1)
        self.is_network_split = False
        self.sync_all()

    def ValidateCoinstakeReward(self, input_amount, output_amount):
        subsidy = input_amount / 250
        if subsidy > maxSubsidy:
            subsidy = maxSubsidy
        expected_output = input_amount + subsidy
        assert_equal(expected_output, output_amount)

    def run_test(self):

        self.sync_blocks()

        # fork activation time: 1647950400
        # set cur_time to 5 hours  before the fork, mine blocks approaching the fork
        cur_time = 1647950400 - (60 * 60 * 5)
        # move back another minute
        cur_time = cur_time - 60
        half_hour = 60 * 30
        self.nodes[0].setmocktime(cur_time)
        self.nodes[1].setmocktime(cur_time)
        # mine initial blocks
        self.nodes[0].generate(1)
        self.sync_blocks()
        self.nodes[1].generate(1)
        self.sync_blocks()
        mined = 0
        while mined < 100:
            self.nodes[0].generate(10)
            self.sync_blocks()
            mined = mined + 10
            cur_time = cur_time + half_hour
            self.nodes[0].setmocktime(cur_time)
            self.nodes[1].setmocktime(cur_time)

        # should now be 1 minute before the fork
        # mine one PoS block to test that it uses pre-fork rewards
        blkhash = self.nodes[0].generatepos(1)[0]
        blockjson = self.nodes[0].getblock(blkhash)
        postx1 = self.nodes[0].gettransaction(blockjson["tx"][1])

        #set the time 2 minutes after the fork
        self.nodes[0].setmocktime(cur_time + 120)
        self.nodes[1].setmocktime(cur_time + 120)
        #mine 6 blocks to get the median time past the fork time
        self.nodes[0].generate(6)
        self.sync_blocks()

        # mine another PoS block to check reward
        blkhash = self.nodes[0].generatepos(1)[0]
        self.sync_blocks()
        blockjson = self.nodes[0].getblock(blkhash)
        postx2 = self.nodes[0].gettransaction(blockjson["tx"][1])

        rawpostx1 = self.nodes[0].getrawtransaction(postx1["txid"], 1)

        # validate the PoS reward for the postfork block followed postfork rules
        rawpostx2 = self.nodes[0].getrawtransaction(postx2["txid"], 1)
        output_amount = 0
        for output in rawpostx2["vout"]:
            output_amount = output_amount + output["value"]
        input_amount = 0
        for input in rawpostx2["vin"]:
            inputtx = self.nodes[0].getrawtransaction(input["txid"], 1)
            input_amount = input_amount + inputtx["vout"][input["vout"]]["value"]

        # check that the Coinstake tx version is either 2 or 4
        if rawpostx2["version"] != 2 and rawpostx2["version"] != 4:
            raise AssertionError("Invalid Coinstake Version")
        self.ValidateCoinstakeReward(input_amount, output_amount)
        # post fork referenceHash in Coinstake must be 0
        assert_equal(int(rawpostx2["referenceHash"]), 0)



if __name__ == '__main__':
    ForkTest().main()


# Create a convenient function for an interactive python debugging session
def Test():
    t = ForkTest()
    bitcoinConf = {
        "debug": ["net", "blk", "thin", "mempool", "req", "bench", "evict"],
        "blockprioritysize": 2000000  # we don't want any transactions rejected due to insufficient fees...
    }

    # you may want these additional flags:
    # "--srcdir=<out-of-source-build-dir>/debug/src"
    flags = standardFlags()
    t.main(flags, bitcoinConf, None)
