#include:
#    - template: Code-Quality.gitlab-ci.yml

#code_quality:
#    artifacts:
#        paths: [gl-code-quality-report.json]

# Top-level general rules determine when this pipeline is run:
# - only on merge requests, new tags and changes to dev
# - NOT on any branch except dev
# - will run detached merge request pipelines for any merge request,
#   targeting any branch
# Read more on when to use this template at
# https://docs.gitlab.com/ee/ci/yaml/#workflowrules
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Workflows/MergeRequest-Pipelines.gitlab-ci.yml

workflow:
    rules:
        - if: $CI_MERGE_REQUEST_ID
        - if: $CI_COMMIT_TAG
        - if: $CI_COMMIT_BRANCH == "dev"

image: ubuntu:18.04

stages:
    - static_checks
    - build_depends
    - build
    - build_tests
    - qa_tests
    - benchmark_tests

cache: &global_cache_settings
    paths:
        - ccache/

.ccache_scripts:
    before_script:
        - set -o errexit; source .gitlab-ci/before_script.sh
        - mkdir -p ccache
        - export CCACHE_BASEDIR=${PWD}
        - export CCACHE_DIR=${PWD}/ccache
        - export CCACHE_COMPILERCHECK=content
        - ccache --zero-stats || true

.cache-linting:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: linting_cache

# Check formatting
check-formatting:
    stage: static_checks
    extends: .cache-linting
    needs: []
    script:
        - set -o errexit; source .gitlab-ci/install.sh
        - apt-get install -y wget gpg-agent
        - wget https://apt.llvm.org/llvm.sh
        - chmod +x llvm.sh
        - ./llvm.sh 12 all #temp fix, the llvm.sh script is currently broken when only given a single arg
        - apt-get install clang-format-12
        - ./autogen.sh
        - ./configure --with-incompatible-bdb --enable-glibc-back-compat --enable-reduce-exports
        - make check-formatting

#x86_64 Linux + deps as via system lib
.cache-debian-nodeps:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: debian_cache-nodeps

build-debian-nodeps:
    stage: build
    extends: .cache-debian-nodeps
    needs: []
    script:
        - set -o errexit; source .gitlab-ci/install.sh
        - ./autogen.sh
        - CONFIG_SITE=$PWD/depends/x86_64-unknown-linux-gnu/share/config.site ./configure --prefix=/
        - make -j4
        - ccache --show-stats
    artifacts:
        paths:
            - ./src/eccoind
            - ./src/test/test_bitcoin
            - ./src/bench/bench_bitcoin

build-debian-tests-nodeps:
    stage: build_tests
    extends: .cache-debian-nodeps
    needs: ["build-debian-nodeps"]
    script:
        - set -o errexit; source .gitlab-ci/install.sh
        - (cd src; ./test/test_bitcoin)
    dependencies:
        - build-debian-nodeps

#bitcoind
.cache-debian:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: debian_cache
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/x86_64-unknown-linux-gnu

build-debian-deps:
    stage: build_depends
    extends: .cache-debian
    needs: []
    script:
        - cd depends
        - make HOST=x86_64-unknown-linux-gnu -j `nproc`
    artifacts:
        paths:
            - depends/x86_64-unknown-linux-gnu

build-debian:
    stage: build
    extends: .cache-debian
    needs: [build-debian-deps]
    script:
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --prefix=$PWD/../depends/x86_64-unknown-linux-gnu --enable-shared --enable-debug --enable-zmq --enable-glibc-back-compat --enable-reduce-exports --cache-file=config.cache
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-debian-deps
    artifacts:
        paths:
            - ./build/src/eccoind
            - ./build/src/test/test_bitcoin
            - ./build/src/bench/bench_bitcoin
            - ./build/qa/*
            - ./qa/*

build-debian-tests:
    stage: build_tests
    extends: .cache-debian
    needs: ["build-debian"]
    script:
        - (cd build/src; ./test/test_bitcoin)
    dependencies:
        - build-debian

test-debian-qa:
    stage: qa_tests
    extends: .cache-debian
    needs: ["build-debian"]
    script:
        - apt-get install -y python3-zmq
        - cd build; ls -l ./qa/pull-tester/rpc-tests.py;
        - ./qa/pull-tester/rpc-tests.py --coverage --no-ipv6-rpc-listen
    dependencies:
        - build-debian

test-debian-benchmarks:
    stage: benchmark_tests
    extends: .cache-debian
    needs: ["build-debian"]
    script:
        - (build/src/bench/bench_bitcoin -evals=1)
    dependencies:
        - build-debian

#test-debian-unittests:
#    stage: test
#    cache: {}
#    needs: ["build-debian-tests"]
#    script:
#        - (cd src; ./test/test_bitcoin --logger=HRF:JUNIT,message,junit_unit_tests.xml)
#    dependencies:
#        - build-debian-tests
#    artifacts:
#        reports:
#            junit: junit_unit_tests.xml


#bitcoind clang (no depend, only system lib installed via apt)
.debian-clang-env:
    extends: .cache-debian-clang
    variables:
        CC: clang-6
        CXX: clang++-6

.cache-debian-clang:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: debian_cache_clang

build-debian-clang:
    stage: build
    extends: .debian-clang-env
    needs: []
    script:
        - cd depends
        - make HOST=x86_64-unknown-linux-gnu -j4
        - cd ..
        - ./autogen.sh
        - CONFIG_SITE=$PWD/depends/x86_64-unknown-linux-gnu/share/config.site ./configure --prefix=/
        - make -j4
        - ccache --show-stats
    artifacts:
        paths:
            - ./src/eccoind
            - ./src/test/test_bitcoin
            - ./src/bench/bench_bitcoin

build-debian-tests-clang:
    stage: build_tests
    extends: .debian-clang-env
    needs: ["build-debian-clang"]
    script:
        - (cd src; ./test/test_bitcoin)
    artifacts:
        paths:
            - .src/test/test_bitcoin
    dependencies:
        - build-debian-clang

#test-debian-unittests-clang:
#    stage: test
#    cache: {}
#    needs: ["build-debian-tests-clang"]
#    script:
#        - (cd src; ./test/test_bitcoin --logger=HRF:JUNIT,message,junit_unit_tests.xml)
#    dependencies:
#        - build-debian-tests-clang
#    artifacts:
#        reports:
#            junit: junit_unit_tests.xml

#test-debian-benchmarks-clang:
#    stage: test
#    extends: .debian-clang-env
#    needs: ["build-debian-clang"]
#    script:
#        - (cd src; ./bench/bench_bitcoin -evals=1)
#    dependencies:
#        - build-debian-clang

#ARM64
.cache-arm-64:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: arm_cache-64
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/aarch64-linux-gnu

build-arm-depends-64:
    stage: build_depends
    extends: .cache-arm-64
    script:
        - apt-get install -y g++-aarch64-linux-gnu curl
        - cd depends
        - make HOST=aarch64-linux-gnu NO_QT=1 JOBS=`nproc`
    artifacts:
        paths:
            - depends/aarch64-linux-gnu

build-arm-64:
    stage: build
    extends: .cache-arm-64
    needs: ["build-arm-depends-64"]
    script:
        - apt-get install -y g++-aarch64-linux-gnu curl
        - ./autogen.sh
        - CONFIG_SITE=$PWD/depends/aarch64-linux-gnu/share/config.site ./configure --prefix=/
        - make -j4
        - ccache --show-stats
    dependencies:
        - build-arm-depends-64

#ARM32
.cache-arm-32:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: arm_cache-32
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/arm-linux-gnueabihf

build-arm-depends-32:
    stage: build_depends
    extends: .cache-arm-32
    script:
        - apt-get install -y g++-arm-linux-gnueabihf curl
        - cd depends
        - make HOST=arm-linux-gnueabihf NO_QT=1 JOBS=`nproc`
    artifacts:
        paths:
            - depends/arm-linux-gnueabih

build-arm-32:
    stage: build
    extends: .cache-arm-32
    needs: ["build-arm-depends-32"]
    script:
        - apt-get install -y g++-arm-linux-gnueabihf curl
        - ./autogen.sh
        - CONFIG_SITE=$PWD/depends/arm-linux-gnueabihf/share/config.site ./configure --prefix=/
        - make -j4
        - ccache --show-stats
    dependencies:
        - build-arm-depends-32

#Win64
.cache-win-64:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: win_cache-64
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/x86_64-w64-mingw32

build-win-64-depends:
    stage: build_depends
    extends: .cache-win-64
    script:
        - apt-get update
        - apt-get install -y python3 nsis g++-mingw-w64-x86-64 wine64 wine-binfmt
        - update-alternatives --set x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++-posix
        - cd depends
        - make HOST=x86_64-w64-mingw32 NO_QT=1 JOBS=`nproc`
    artifacts:
        paths:
            - depends/x86_64-w64-mingw32

build-win-64:
    stage: build
    extends: .cache-win-64
    needs: ["build-win-64-depends"]
    script:
        - apt-get update
        - apt-get install -y python3 nsis g++-mingw-w64-x86-64 wine64 wine-binfmt
        - update-alternatives --set x86_64-w64-mingw32-g++ /usr/bin/x86_64-w64-mingw32-g++-posix
        - ./autogen.sh
        - CONFIG_SITE=$PWD/depends/x86_64-w64-mingw32/share/config.site ./configure --prefix=/
        - make -j4
        - ccache --show-stats
    dependencies:
        - build-win-64-depends

#Win32
.cache-win-32:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: win_cache-32
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/i686-w64-mingw32

build-win-32-depends:
    stage: build_depends
    extends: .cache-win-32
    script:
        - dpkg --add-architecture i386
        - apt-get update
        - apt-get install -y python3 nsis g++-mingw-w64-i686 wine32 wine-binfmt
        - update-alternatives --set i686-w64-mingw32-g++ /usr/bin/i686-w64-mingw32-g++-posix
        - cd depends
        - make HOST=i686-w64-mingw32 NO_QT=1 JOBS=`nproc`
    artifacts:
        paths:
            - depends/i686-w64-mingw32

build-win-32:
    stage: build
    extends: .cache-win-32
    needs: ["build-win-32-depends"]
    script:
        - dpkg --add-architecture i386
        - apt-get update
        - apt-get install -y python3 nsis g++-mingw-w64-i686 wine32 wine-binfmt
        - update-alternatives --set i686-w64-mingw32-g++ /usr/bin/i686-w64-mingw32-g++-posix
        - ./autogen.sh
        - CONFIG_SITE=$PWD/depends/i686-w64-mingw32/share/config.site ./configure --prefix=/
        - make -j4
        - ccache --show-stats
    dependencies:
        - build-win-32-depends

#Linux32-bit
.cache-debian-32:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: debian_cache-32
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/i686-pc-linux-gnu

build-debian-32-depends:
    stage: build_depends
    extends: .cache-debian-32
    script:
        - dpkg --add-architecture i386
        - apt-get update
        - apt-get install -y python3 g++-multilib bc python3-zmq
        - cd depends
        - make HOST=i686-pc-linux-gnu -j `nproc`
    artifacts:
        paths:
            - depends/i686-pc-linux-gnu

build-debian-32:
    stage: build
    extends: .cache-debian-32
    needs: ["build-debian-32-depends"]
    script:
        - dpkg --add-architecture i386
        - apt-get update
        - apt-get install -y python3 g++-multilib bc python3-zmq
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --prefix=/$PWD/../depends/i686-pc-linux-gnu  --enable-zmq --enable-glibc-back-compat --enable-reduce-exports LDFLAGS=-static-libstdc++
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-debian-32-depends
    artifacts:
        paths:
            - ./build/src/eccoind
            - ./build/src/test/test_bitcoin
            - ./build/src/bench/bench_bitcoin
            - ./build/qa/*
            - ./qa/*

# debian 32 is not able to execute the test or qa files even though it is able to find
# skip these tests for now
#build-debian-tests-32:
#    stage: build_tests
#    extends: .cache-debian-32
#    needs: ["build-debian-32"]
#    script:
#        - cd build/src; pwd; ls -la test/test_bitcoin; test/test_bitcoin
#    dependencies:
#        - build-debian-32
#
#test-debian-qa-32:
#    stage: qa_tests
#    extends: .cache-debian-32
#    needs: ["build-debian-32"]
#    script:
#        - apt-get install -y python3 python3-zmq
#        - cd build; pwd; ls -la qa/pull-tester/rpc-tests.py; ./qa/pull-tester/rpc-tests.py --coverage --no-ipv6-rpc-listen
#    dependencies:
#        - build-debian-32

#Cross-Mac
.cache-osx:
    extends: .ccache_scripts
    cache:
        <<: *global_cache_settings
        key: osx_cache
        paths:
            - ccache/
            - depends/sources
            - depends/built
            - depends/x86_64-apple-darwin11

build-osx-depends:
    stage: build_depends
    extends: .cache-osx
    script:
        - apt-get update
        - apt-get install -y cmake imagemagick libcap-dev librsvg2-bin libz-dev libbz2-dev libtiff-tools python-dev python3-setuptools-git clang clang-tools
        - mkdir -p depends/sdk-sources; mkdir depends/SDKs
        - curl --location --fail https://www.bitcoinunlimited.info/sdks/MacOSX10.11.sdk.tar.gz -o ./depends/sdk-sources/MacOSX10.11.sdk.tar.gz
        - tar -C depends/SDKs -xf depends/sdk-sources/MacOSX10.11.sdk.tar.gz
        - cd depends
        - make HOST=x86_64-apple-darwin11 JOBS=`nproc`
    artifacts:
        paths:
            - depends


build-osx:
    stage: build
    extends: .cache-osx
    needs: ["build-osx-depends"]
    script:
        - apt-get install -y cmake imagemagick libcap-dev librsvg2-bin libz-dev libbz2-dev libtiff-tools python-dev python3-setuptools-git clang clang-tools
        - ./autogen.sh
        - mkdir build; cd build
        - ../configure --enable-reduce-exports CXX=/usr/bin/clang++ CC=/usr/bin/clang --prefix=$PWD/../depends/x86_64-apple-darwin11
        - make -j `nproc`
        - ccache --show-stats
    dependencies:
        - build-osx-depends
    artifacts:
        when: on_failure
        paths:
            - build/config.log
